
function ade(val) {
    if (val == 2) {
        $('.don').show();
    }
    if (val == 1) {
        $('.don').hide();
    }

    if (val != "") {
        $('.aob,.ec').show();
        $('.desf1').show();

    } else {
        $('.don,.aob,.eabn,.ec,.afec,.pdl,.aac,.ais,.sor,.ap').hide();
        $('.desf1').hide();
    }
}

function entr(val) {
    if (val == 'Name Not Listed') {
        $('.eabn').show();
    } else
    {
        $('.eabn').hide();
    }
}

function pacpcfn(val) {
    if (val == 'Not Listed') {
        $('.pacpcnl').show();
    } else {
        $('.pacpcnl').hide();
    }
}

function ciicfn(val) {
    if (val == 'Other') {
        $('.ciico').show();
    }
    else {
        $('.ciico').hide();
    }
}

function aftec(val) {
    if (val == 'Referral') {
        $('.sor').show();
    } else
    {
        $('.sor').hide();
    }

    if (val == 'Not Listed') {
        $('.sohaufu').show();
    } else
    {
        $('.sohaufu').hide();
    }

    if (val != '') {
        $('.tai').show();
        $('.desf3').show();
    } else {
        $('.tai').hide();
        $('.desf3').hide();
    }
}

function taifn() {

    var apArr = new Array();
    $.each($("input[name='tai[]']:checked"), function () {
        apArr.push($(this).val());
    });

    if (apArr.length > 0) {



        $('.desf4').show();

        if (($.inArray('New Application', apArr) > -1) || ($.inArray('Broker of Record (BOR)', apArr) > -1) || ($.inArray("Didn't Buy/Follow Up Needed", apArr) > -1)) {
            $('.ap').show();
        }
        else {
            $('.ap').hide();
        }

        if (($.inArray('Existing Application', apArr) > -1)) {
            $('.eapp').show();

            var appArr = new Array();
            $.each($("input[name='eapp[]']:checked"), function () {
                appArr.push($(this).val());
            });
            if (appArr.length > 0) {
                if (($.inArray('Purchase New Products', appArr) > -1)) {
                    $('.ap').show();
                }

                if (($.inArray('Update Covered California application', appArr) > -1) && appArr.length == 1 && apArr.length == 1) {
                    $('.desf4').hide();
                }
            }
            else if ($.inArray('Existing Application', apArr) == 0 && apArr.length == 1) {
                $('.desf4').hide();
            }
        }
        else {
            $('.eapp').hide();
        }
    }
    else {
        $('.ap,.eapp').hide();
        $('.desf4').hide();
    }
}

function eappfn() {

    var apArr = new Array();
    $.each($("input[name='eapp[]']:checked"), function () {
        apArr.push($(this).val());
    });

    if (apArr.length > 0) {

        if (($.inArray('Purchase New Products', apArr) > -1)) {
            $('.ap').show();
            $('.desf4').show();

        }
        else {
            taifn();
        }
    }
    else {
        taifn();
    }
}

function tnoaohtffn(val) {

    if (val == 'Not Listed') {
        $('.hmiyhnl').show();
    } else
    {
        $('.hmiyhnl').hide();
    }

}

function tdupdate() {

    var tdmv = $('#tdm').val();
    if (tdmv == "January")
        tdmvu = '01';
    if (tdmv == "February")
        tdmvu = '02';
    if (tdmv == "March")
        tdmvu = '03';
    if (tdmv == "April")
        tdmvu = '04';
    if (tdmv == "May")
        tdmvu = '05';
    if (tdmv == "June")
        tdmvu = '06';
    if (tdmv == "July")
        tdmvu = '07';
    if (tdmv == "August")
        tdmvu = '08';
    if (tdmv == "September")
        tdmvu = '09';
    if (tdmv == "October")
        tdmvu = '10';
    if (tdmv == "November")
        tdmvu = '11';
    if (tdmv == "December")
        tdmvu = '12';

    if ($('#tdm').val() != "" && $('#tdt').val() != "" && $('#tdy').val() != "") {
        var startDate = tdmvu + '-' + $('#tdt').val() + '-' + $('#tdy').val();
        $("#StartDate").datepicker("setDate", startDate);
    }
}

function padobuppdate() {
    if ($('#padobm').val() != "" && $('#padobd').val() != "" && $('#padoby').val() != "") {
        var pdobdt = $('#padobm').val().toString() + "-" + $('#padobd').val().toString() + "-" + $('#padoby').val().toString();
        $("#padob").datepicker("setDate", pdobdt);
    }

}

function sdobupdate() {
    if ($('#sdobm').val() != "" && $('#sdobd').val() != "" && $('#sdoby').val() != "") {
        var sdobdt = $('#sdobm').val().toString() + "-" + $('#sdobd').val().toString() + "-" + $('#sdoby').val().toString();
        $("#sdob").datepicker("setDate", sdobdt);
    }
}

function apddupdate() {
    if ($('#apddm').val() != "" && $('#apddd').val() != "" && $('#apddy').val() != "") {
        var psddt = $('#apddm').val().toString() + "-" + $('#apddd').val().toString() + "-" + $('#apddy').val().toString();
        $("#apdddp").datepicker("setDate", psddt);
    }
}

function psdupdate() {
    if ($('#psdm').val() != "" && $('#psdd').val() != "" && $('#psdy').val() != "") {
        var psddt = $('#psdm').val().toString() + "-" + $('#psdd').val().toString() + "-" + $('#psdy').val().toString();
        $("#psddp").datepicker("setDate", psddt);
    }
}

function sdrdocghupdate() {
    if ($('#sdrdocghm').val() != "" && $('#sdrdocghd').val() != "" && $('#sdrdocghy').val() != "") {
        var psddt = $('#sdrdocghm').val().toString() + "-" + $('#sdrdocghd').val().toString() + "-" + $('#sdrdocghy').val().toString();
        $("#sdrdocghdp").datepicker("setDate", psddt);
    }
}

function sdocdupdate() {
    if ($('#sdocdm').val() != "" && $('#sdocdd').val() != "" && $('#sdocdy').val() != "") {
        var psddt = $('#sdocdm').val().toString() + "-" + $('#sdocdd').val().toString() + "-" + $('#sdocdy').val().toString();
        $("#sdocddp").datepicker("setDate", psddt);
    }
}


function ghrdocupdate() {
    if ($('#ghrdocm').val() != "" && $('#ghrdocd').val() != "" && $('#ghrdocy').val() != "") {
        var psddt = $('#ghrdocm').val().toString() + "-" + $('#ghrdocd').val().toString() + "-" + $('#ghrdocy').val().toString();
        $("#ghrdocdp").datepicker("setDate", psddt);
    }
}

function pedveupdate() {
    if ($('#pedvem').val() != "" && $('#pedved').val() != "" && $('#pedvey').val() != "") {
        var psddt = $('#pedvem').val().toString() + "-" + $('#pedved').val().toString() + "-" + $('#pedvey').val().toString();
        $("#pedvedp").datepicker("setDate", psddt);
    }
}

function pedaroeupdate() {
    if ($('#pedaroem').val() != "" && $('#pedaroed').val() != "" && $('#pedaroey').val() != "") {
        var psddt = $('#pedaroem').val().toString() + "-" + $('#pedaroed').val().toString() + "-" + $('#pedaroey').val().toString();
        $("#pedaroedp").datepicker("setDate", psddt);
    }
}

function pedaciupdate() {
    if ($('#pedacim').val() != "" && $('#pedacid').val() != "" && $('#pedaciy').val() != "") {
        var psddt = $('#pedacim').val().toString() + "-" + $('#pedacid').val().toString() + "-" + $('#pedaciy').val().toString();
        $("#pedacidp").datepicker("setDate", psddt);
    }
}

function pedaleupdate() {
    if ($('#pedalem').val() != "" && $('#pedaled').val() != "" && $('#pedaley').val() != "") {
        var psddt = $('#pedalem').val().toString() + "-" + $('#pedaled').val().toString() + "-" + $('#pedaley').val().toString();
        $("#pedaledp").datepicker("setDate", psddt);
    }
}

function pedameupdate() {
    if ($('#pedamem').val() != "" && $('#pedamed').val() != "" && $('#pedamey').val() != "") {
        var psddt = $('#pedamem').val().toString() + "-" + $('#pedamed').val().toString() + "-" + $('#pedamey').val().toString();
        $("#pedamedp").datepicker("setDate", psddt);
    }
}

function pedaoeupdate() {
    if ($('#pedaoem').val() != "" && $('#pedaoed').val() != "" && $('#pedaoey').val() != "") {
        var psddt = $('#pedaoem').val().toString() + "-" + $('#pedaoed').val().toString() + "-" + $('#pedaoey').val().toString();
        $("#pedaoedp").datepicker("setDate", psddt);
    }
}

function ddobupdate_1() {
    if ($('#ddobm_1').val() != "" && $('#ddobd_1').val() != "" && $('#ddoby_1').val() != "") {
        var psddt = $('#ddobm_1').val().toString() + "-" + $('#ddobd_1').val().toString() + "-" + $('#ddoby_1').val().toString();
        $("#ddobdp_1").datepicker("setDate", psddt);
    }
}

function ddobupdate_2() {
    if ($('#ddobm_2').val() != "" && $('#ddobd_2').val() != "" && $('#ddoby_2').val() != "") {
        var psddt = $('#ddobm_2').val().toString() + "-" + $('#ddobd_2').val().toString() + "-" + $('#ddoby_2').val().toString();
        $("#ddobdp_2").datepicker("setDate", psddt);
    }
}

function ddobupdate_3() {
    if ($('#ddobm_3').val() != "" && $('#ddobd_3').val() != "" && $('#ddoby_3').val() != "") {
        var psddt = $('#ddobm_3').val().toString() + "-" + $('#ddobd_3').val().toString() + "-" + $('#ddoby_3').val().toString();
        $("#ddobdp_3").datepicker("setDate", psddt);
    }
}

function ddobupdate_4() {
    if ($('#ddobm_4').val() != "" && $('#ddobd_4').val() != "" && $('#ddoby_4').val() != "") {
        var psddt = $('#ddobm_4').val().toString() + "-" + $('#ddobd_4').val().toString() + "-" + $('#ddoby_4').val().toString();
        $("#ddobdp_4").datepicker("setDate", psddt);
    }
}

function ddobupdate_5() {
    if ($('#ddobm_5').val() != "" && $('#ddobd_5').val() != "" && $('#ddoby_5').val() != "") {
        var psddt = $('#ddobm_5').val().toString() + "-" + $('#ddobd_5').val().toString() + "-" + $('#ddoby_5').val().toString();
        $("#ddobdp_5").datepicker("setDate", psddt);
    }
}


function entrcenterchk() {
    var ecvArr = new Array();
    $.each($("input[name='entr_center[]']:checked"), function () {
        ecvArr.push($(this).val());
    });

    if (ecvArr.length > 0) {
        $('.afec,.pdl').show();
        $('.desf2').show();

    }
    else {
        $('.afec,.pdl,.sor').hide();
        $('.desf2').hide();
    }
}

function seaffn() {
    if ($('#seaf').is(':checked')) {
        $('#sma1').val($('#apma1').val());
        $('#sma2').val($('#apma2').val());
        $('#smacity').val($('#apmacity').val());
        $('#smastate').val($('#apmastate').val());
        $('#smazipcode').val($('#apmazipcode').val());
    }
}

function apraffn() {

    if ($('#apraf').is(':checked')) {
        $('#apfnn').val($('#pafnm').val());
        $('#aplnn').val($('#palnm').val());
        $('.pann').text("Primary Applicant's Name");
    }
    else {
        $('.pann').text("Primary Applicant's Name (New)");
    }
}

function earfn() {

    if ($('#epraf').is(':checked')) {
        $('#apfne').val($('#pafnm').val());
        $('#aplne').val($('#palnm').val());
    }
}

function anfn() {

    //~ New Applicant's Registration
    if ($('#apfnn').length)
        $('#apfnn').val($('#pafnm').val());
    if ($('#aplnn').length)
        $('#aplnn').val($('#palnm').val());

    //~ Existing Applicant(s) Registration
    if ($('#apfne').length)
        $('#apfne').val($('#pafnm').val());
    if ($('#aplne').length)
        $('#aplne').val($('#palnm').val());

    //~ BOR Change Request
    if ($('#apfnbor').length)
        $('#apfnbor').val($('#pafnm').val());
    if ($('#aplnbor').length)
        $('#aplnbor').val($('#palnm').val());

    //~ Group Health Enrollment
    if ($('#apocfngh').length)
        $('#apocfngh').val($('#pafnm').val());
    if ($('#apoclngh').length)
        $('#apoclngh').val($('#palnm').val());

    //~ Dental Enrollment
    if ($('#pafnd').length)
        $('#pafnd').val($('#pafnm').val());
    if ($('#palnd').length)
        $('#palnd').val($('#palnm').val());

    //~ Vision Enrollment
    if ($('#pafnv').length)
        $('#pafnv').val($('#pafnm').val());
    if ($('#palnv').length)
        $('#palnv').val($('#palnm').val());

    //~ Accident Enrollment
    if ($('#pafna').length)
        $('#pafna').val($('#pafnm').val());
    if ($('#palna').length)
        $('#palna').val($('#palnm').val());

    //~ Critical Illness Enrollment
    if ($('#pafnci').length)
        $('#pafnci').val($('#pafnm').val());
    if ($('#palnci').length)
        $('#palnci').val($('#palnm').val());

    //~ Life Enrollment
    if ($('#pafnle').length)
        $('#pafnle').val($('#pafnm').val());
    if ($('#palnle').length)
        $('#palnle').val($('#palnm').val());

    //~ Medicare Enrollment
    if ($('#pafnme').length)
        $('#pafnme').val($('#pafnm').val());
    if ($('#palnme').length)
        $('#palnme').val($('#palnm').val());

    //~ Other Enrollment
    if ($('#pafnoe').length)
        $('#pafnoe').val($('#pafnm').val());
    if ($('#palnoe').length)
        $('#palnoe').val($('#palnm').val());
}

function apchk() {
    var apArr = new Array();
    $.each($("input[name='a_p[]']:checked"), function () {
        apArr.push($(this).val());
    });

    if (apArr.length > 0) {
        $('.desf5').show();
        $('.aac').show();

        if ($.inArray('Group Health', apArr) > -1 && apArr.length == 1) {
            $('.desf6').hide();
        } else {
            $('.desf6').show();
        }

        if ($.inArray('Individual & Family Health', apArr) > -1) {
            $('.tnoaohtf').show();
            if ($('#tnoaohtf').val() == "Not Listed")
                $('.hmiyhnl').show();
        } else {
            $('.tnoaohtf,.hmiyhnl').hide();
        }

        if ($.inArray('Other', apArr) > -1) {
            $('.opp').show();
        } else {
            $('.opp').hide();
        }

    }
    else {
        $('.aac,.ais,.tnoaohtf,.opp,.hmiyhnl').hide();
        $('.desf5').hide();
    }
}


function dicdefn(val) {
    if (val == "Other") {
        $('.dicdeo').show();
    } else {
        $('.dicdeo').hide();
    }
}


function acafn(val) {
    if (val == "Other") {
        $('.acao').show();
    } else {
        $('.acao').hide();
    }
}


function micmefn(val) {

    if (val == "Other") {
        $('.micmeo').show();
    } else {
        $('.micmeo').hide();
    }
}

function vicvnfn(val) {

    if (val == "Other") {
        $('.vicvno').show();
    } else {
        $('.vicvno').hide();
    }
}

function witarttecfn() {
    var apArr = new Array();
    $.each($("input[name='witarttec[]']:checked"), function () {
        apArr.push($(this).val());
    });

    if (apArr.length > 0) {


        if ($.inArray('Upgrade or downgrade their coverage', apArr) > -1) {
            $('.atli').show();
        }
        else {
            $('.atli').hide();
        }

        if ($.inArray('Change their health insurance carrier', apArr) > -1) {
            $('.anic').show();
            if ($('#anic').val() == "Not Listed") {
                $('.epaicnl').show();
            }
        }
        else {
            $('.anic,.epaicnl').hide();
        }

        if (($.inArray('Change their health insurance carrier', apArr) > -1) && ($.inArray('Upgrade or downgrade their coverage', apArr) == -1)) {
            var h1txt = $(".anic_hd").text();
            $(".anic_hd").text(h1txt.replace('1.2', '1.1'));
            var h2txt = $(".epaicnl_hd").text();
            $(".epaicnl_hd").text(h2txt.replace('1.2', '1.1'));
        }
        else {
            var htxt = $(".anic_hd").text();
            $(".anic_hd").text(htxt.replace('1.1', '1.2'));
            var h2txt = $(".epaicnl_hd").text();
            $(".epaicnl_hd").text(h2txt.replace('1.1', '1.2'));
        }

    }
    else {

        $('.anic,.atli,.epaicnl').hide();
    }
}
function sdofcfn() {
    var apArr = new Array();
    $.each($("input[name='sdofc[]']:checked"), function () {
        apArr.push($(this).val());
    });

    if (apArr.length > 0) {


        if ($.inArray('Start Date of Coverage', apArr) > -1) {
            $('.sdrdocgh').show();
        }
        else {
            $('.sdrdocgh').hide();
        }

        if ($.inArray('Renewal Date of Coverage', apArr) > -1) {
            $('.ghrdoc').show();
        }
        else {
            $('.ghrdoc').hide();
        }

        if (apArr.length == 1) {
            var h1txt = $(".rdoc_hd").text();
            $(".rdoc_hd").text(h1txt.replace('5.2', '5.1'));
        }
        else {
            var htxt = $(".rdoc_hd").text();
            $(".rdoc_hd").text(htxt.replace('5.1', '5.2'));
        }

    }
    else {

        $('.ghrdoc,.sdrdocgh').hide();
    }
}

function catafrbccfaachk() {
    var catafrbccfaaArr = new Array();
    $.each($("input[name='catafrbccfaa[]']:checked"), function () {
        catafrbccfaaArr.push($(this).val());
    });

    if (catafrbccfaaArr.length > 0) {

        if ($.inArray('Proof of Citizenship', catafrbccfaaArr) > -1) {
            $('.ccrpocfwa,.caatcnhnptrpocd').show();
        }
        else {
            $('.ccrpocfwa,.caatcnhnptrpocd').hide();
        }

        if ($.inArray('Proof of Income', catafrbccfaaArr) > -1) {
            $('.ccrpoifwp,.caatcnhnptrpoid').show();
        }
        else {
            $('.ccrpoifwp,.caatcnhnptrpoid').hide();
        }

        if ($.inArray('Proof of Min Essential Coverage', catafrbccfaaArr) > -1) {
            $('.ccrpomecfwa,.caatcnhnptrpomecd').show();
        }
        else {
            $('.ccrpomecfwa,.caatcnhnptrpomecd').hide();
        }


        if ($.inArray('Proof of Non-Incarceration', catafrbccfaaArr) > -1) {
            $('.ccrponifwa,.caatcnhnptrponid,.caatcnhnptrod').show();
        }
        else {
            $('.ccrponifwa,.caatcnhnptrponid,.caatcnhnptrod').hide();
        }


    }
    else {
        $('.ccrpocfwa,.caatcnhnptrpocd,.ccrpoifwp,.caatcnhnptrpoid,.ccrpomecfwa,.caatcnhnptrpomecd,.ccrponifwa,.caatcnhnptrponid,.caatcnhnptrod').hide();
    }
}
function eardfn() {
    var catafrbccfaaArr = new Array();
    $.each($("input[name='eard[]']:checked"), function () {
        catafrbccfaaArr.push($(this).val());
    });

    if (catafrbccfaaArr.length > 0) {

        if ($.inArray('Proof of Citizenship', catafrbccfaaArr) > -1) {
            $('.aprccrpocfwa,.aprcaatcnhnptrpocd').show();
        }
        else {
            $('.aprccrpocfwa,.aprcaatcnhnptrpocd').hide();
        }

        if ($.inArray('Proof of Income', catafrbccfaaArr) > -1) {
            $('.aprccrpoifwp,.aprcaatcnhnptrpoid').show();
        }
        else {
            $('.aprccrpoifwp,.aprcaatcnhnptrpoid').hide();
        }

        if ($.inArray('Proof of Min Essential Coverage', catafrbccfaaArr) > -1) {
            $('.aprccrpomecfwa,.aprcaatcnhnptrpomecd').show();
        }
        else {
            $('.aprccrpomecfwa,.aprcaatcnhnptrpomecd').hide();
        }


        if ($.inArray('Proof of Non-Incarceration', catafrbccfaaArr) > -1) {
            $('.aprccrponifwa,.aprcaatcnhnptrponid').show();
        }
        else {
            $('.aprccrponifwa,.aprcaatcnhnptrponid').hide();
        }
    }
    else {
        $('.aprccrpocfwa,.aprcaatcnhnptrpocd').hide();
        $('.aprccrpoifwp,.aprcaatcnhnptrpoid').hide();
        $('.aprccrpomecfwa,.aprcaatcnhnptrpomecd').hide();
        $('.aprccrponifwa,.aprcaatcnhnptrponid').hide();
    }
}



function dtpauamnchk(val) {
    if (val == "Yes") {
        $('.pamn').show();
    }
    else {
        $('.pamn').hide();
    }
}

function hadrccbufn(val) {
    if (val == "No") {
        $('.cataftasofaa').show();
        cataftasofaafn();
    }
    else {
        $('.cataftasofaa').hide();
        $('.cpocdaofwa,.ipoidaofwa,.epomecdaofwa,.nponisdaofwa').hide();
    }
}

function paeaofn(val) {
    if (val == "Yes") {
        $('.paea').show();
    }
    else {
        $('.paea').hide();
    }
}

function spousesemailofn(val) {
    if (val == "Yes") {
        $('.spousesemail').show();
    }
    else {
        $('.spousesemail').hide();
    }
}

function dtsuhamnchk(val) {
    if (val == "Yes") {
        $('.smn').show();
    }
    else {
        $('.smn').hide();
    }
}

function papniacpolchk(val) {
    if (val == "Cell Phone") {
        $('.pacpc').show();
    }
    else {
        $('.pacpc,.dtpahadcpn,.paapn,.apaapn,.paacpc').hide();
    }
}

function paapnchk(val) {
    if (val == "Cell Phone") {
        $('.paacpc').show();
    }
    else {
        $('.paacpc').hide();
    }
}

function pacpcchk(val) {
    if (val != "") {
        $('.dtpahadcpn').show();
    }
    else {
        $('.dtpahadcpn,.paapn,.apaapn,.paacpc').hide();
    }
}

function dtpahadcpnchk(val) {
    if (val == "Yes") {
        $('.paapn,.apaapn').show();
    }
    else {
        $('.paapn,.apaapn,.paacpc').hide();
    }
}

function wtborscitccwfn(val) {
    if (val == "No") {
        $('.wwtbnscwcc').show();
    }
    else {
        $('.wwtbnscwcc').hide();
    }
}

function paicfn(val) {
    var coverage_applicants = Array();
    if ($(".aac").css('display') == 'block' && $(".desf4").css('display') == 'block') {
        $("input[name='dpdnt[]']:checked").each(function () {
            coverage_applicants.push($(this).val());
        });
    }
    var otherCoverage = 0;
    for (i = 0; i < coverage_applicants.length; i++) {
        var coverageNm = coverage_applicants[i];
        if (coverageNm.toLowerCase().indexOf("dependent") >= 0) {
            otherCoverage = 1;
        }
    }
    if (val != "") {
        if (val == "Medi-Cal") {
            if (otherCoverage) {
                $('.domc').show();
            }
            $('.dtapftmito,.patli,.dtasufap,.atmpws,.asmpas,.psd,.apdd,.paicnl').hide();
        }
        else if (val == "Other") {
            $('.patli,.paicnl').show();
            if (otherCoverage) {
                $('.domc').show();
            }
            $('.dtapftmito,.dtasufap,.atmpws,.asmpas,.psd,.apdd').hide();
        }
        else {
            if (otherCoverage) {
                $('.domc').show();
            }
            $('.dtapftmito,.patli,.dtasufap,.atmpws,.asmpas,.psd,.apdd').show();
            $('.paicnl').hide();
        }
    }
    else {
        $('.dtapftmito,.patli,.dtasufap,.atmpws,.asmpas,.psd,.apdd,.paicnl,.domc').hide();
    }
}

function aicborfn(val) {
    if (val == "Other") {
        $('.hicnl').show();
    }
    else {
        $('.hicnl').hide();
    }

}

function liclefn(val) {
    if (val == "Other") {
        $('.licleo').show();
    }
    else {
        $('.licleo').hide();
    }

}

function anicfn(val) {
    if (val != "") {

        if (val == "Not Listed") {
            $('.epaicnl').show();
        }
        else {
            $('.epaicnl').hide();
        }
    }
    else {
        $('.epaicnl').hide();
    }
}


function dccraadftafn(val) {
    if (val == "Yes") {
        $('.catafrbccfaa').show();
        catafrbccfaachk();
    }
    else {

        $('.catafrbccfaa,.ccrpocfwa,.caatcnhnptrpocd,.ccrpoifwp,.caatcnhnptrpoid,.ccrpomecfwa,.caatcnhnptrpomecd,.ccrponifwa,.caatcnhnptrponid,.caatcnhnptrod').hide();
    }
}

function aprdrfn(val) {
    if (val == "Yes") {

        $('.apr_documents_requested').show();
    }
    else {

        $('.apr_documents_requested').hide();
    }
}

function nardrfn(val) {
    if (val == "Yes") {

        $('.nar_documents_requested').show();
    }
    else {

        $('.nar_documents_requested').hide();
    }
}

function prvsfn(val) {
    var viewfrm = val - 1;
    if ($("#enrlform" + viewfrm.length)) {
        $('[id^="enrlform"]').hide();
        $('#enrlform' + viewfrm).show();
    }
    else {
        $('[id^="enrlform"]').hide();
        $('#enrlform' + val).show();
    }
    window.scrollTo(0, 0);
}

function nextfn(val) {
    var viewfrm = val + 1;

    if ($("#enrlform" + viewfrm).length) {
        $('[id^="enrlform"]').hide();
        $('#enrlform' + viewfrm).show();
    }
    else {

        $('[id^="enrlform"]').hide();
        $('#enrlform' + val).show();
    }
    window.scrollTo(0, 0);
}


$(document).ready(function () {

    var dpmonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    if ($('#ddobdp_1').length) {
        $('#ddobdp_1').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("ddoby_1").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("ddoby_1").options[$('#ddoby_1 option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#ddobm_1').val(month);
            $('#ddobd_1').val(day);
            $('#ddoby_1').val(year);
        });
    }


    if ($('#ddobdp_2').length) {
        $('#ddobdp_2').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("ddoby_2").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("ddoby_2").options[$('#ddoby_2 option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#ddobm_2').val(month);
            $('#ddobd_2').val(day);
            $('#ddoby_2').val(year);
        });
    }


    if ($('#ddobdp_3').length) {
        $('#ddobdp_3').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("ddoby_3").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("ddoby_3").options[$('#ddoby_3 option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#ddobm_3').val(month);
            $('#ddobd_3').val(day);
            $('#ddoby_3').val(year);
        });
    }



    if ($('#ddobdp_4').length) {
        $('#ddobdp_4').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("ddoby_4").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("ddoby_4").options[$('#ddoby_4 option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#ddobm_4').val(month);
            $('#ddobd_4').val(day);
            $('#ddoby_4').val(year);
        });
    }


    if ($('#ddobdp_5').length) {
        $('#ddobdp_5').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("ddoby_5").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("ddoby_5").options[$('#ddoby_5 option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#ddobm_5').val(month);
            $('#ddobd_5').val(day);
            $('#ddoby_5').val(year);
        });
    }

    if ($('#sdob').length) {
        $('#sdob').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("sdoby").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("sdoby").options[$('#sdoby option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#sdobm').val(month);
            $('#sdobd').val(day);
            $('#sdoby').val(year);
        });
    }

    if ($('#pedaoedp').length) {
        $('#pedaoedp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("pedaoey").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("pedaoey").options[$('#pedaoey option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#pedaoem').val(month);
            $('#pedaoed').val(day);
            $('#pedaoey').val(year);
        });
    }


    if ($('#pedamedp').length) {
        $('#pedamedp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("pedamey").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("pedamey").options[$('#pedamey option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#pedamem').val(month);
            $('#pedamed').val(day);
            $('#pedamey').val(year);
        });
    }


    if ($('#pedaledp').length) {
        $('#pedaledp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("pedaley").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("pedaley").options[$('#pedaley option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#pedalem').val(month);
            $('#pedaled').val(day);
            $('#pedaley').val(year);
        });
    }



    if ($('#pedacidp').length) {
        $('#pedacidp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("pedaciy").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("pedaciy").options[$('#pedaciy option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#pedacim').val(month);
            $('#pedacid').val(day);
            $('#pedaciy').val(year);
        });
    }



    if ($('#pedaroedp').length) {
        $('#pedaroedp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("pedaroey").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("pedaroey").options[$('#pedaroey option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#pedaroem').val(month);
            $('#pedaroed').val(day);
            $('#pedaroey').val(year);
        });
    }

    if ($('#pedvedp').length) {
        $('#pedvedp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("pedvey").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("pedvey").options[$('#pedvey option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#pedvem').val(month);
            $('#pedved').val(day);
            $('#pedvey').val(year);
        });
    }

    if ($('#ghrdocdp').length) {
        $('#ghrdocdp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("ghrdocy").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("ghrdocy").options[$('#ghrdocy option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#ghrdocm').val(month);
            $('#ghrdocd').val(day);
            $('#ghrdocy').val(year);
        });
    }

    if ($('#sdocddp').length) {
        $('#sdocddp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("sdocdy").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("sdocdy").options[$('#sdocdy option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#sdocdm').val(month);
            $('#sdocdd').val(day);
            $('#sdocdy').val(year);
        });
    }

    if ($('#sdrdocghdp').length) {
        $('#sdrdocghdp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("sdrdocghy").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("sdrdocghy").options[$('#sdrdocghy option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#sdrdocghm').val(month);
            $('#sdrdocghd').val(day);
            $('#sdrdocghy').val(year);
        });
    }

    if ($('#padob').length) {
        $('#padob').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("padoby").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("padoby").options[$('#padoby option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#padobm').val(month);
            $('#padobd').val(day);
            $('#padoby').val(year);
        });
    }


    if ($('#psddp').length) {
        $('#psddp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("psdy").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("psdy").options[$('#psdy option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#psdm').val(month);
            $('#psdd').val(day);
            $('#psdy').val(year);
        });
    }

    if ($('#apdddp').length) {
        $('#apdddp').datepicker({
            orientation: 'bottom',
            autoclose: true,
            startDate: new Date(document.getElementById("apddy").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("apddy").options[$('#apddy option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

            if (day.toString().length == 1)
                day = '0' + day;
            if (month.toString().length == 1)
                month = '0' + month;
            $('#apddm').val(month);
            $('#apddd').val(day);
            $('#apddy').val(year);
        });
    }

    if ($('#StartDate').length) {
        $('#StartDate').datepicker({
            orientation: 'bottom',
            autoclose: true,
            todayHighlight: true,
            startDate: new Date(document.getElementById("tdy").options[1].text + '-01-01'),
            endDate: new Date(document.getElementById("tdy").options[$('#tdy option').length - 1].text + '-12-31'),
        }).on('changeDate', function (dateText, inst) {
            var date = $(this).datepicker('getDate'),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();
            var sm = dpmonths[date.getMonth()];
            if (day.toString().length == 1)
                day = '0' + day;
            $('#tdm').val(sm);
            $('#tdt').val(day);
            $('#tdy').val(year);
        });
    }


    var today_ = new Date();
    var dd_ = today_.getDate();
    var mm_ = today_.getMonth() + 1;
    var yyyy_ = today_.getFullYear();

    if (dd_ < 10) {
        dd_ = '0' + dd_;
    }

    if (mm_ < 10) {
        mm_ = '0' + mm_;
    }
    var sm_ = dpmonths[today_.getMonth()];
    if ($('#tdm').val() == "") {
        $('#tdm').val(sm_);
    }
    if ($('#tdt').val() == "") {
        $('#tdt').val(dd_);
    }
    if ($('#tdy').val() == "") {
        $('#tdy').val(yyyy_);
    }
});
