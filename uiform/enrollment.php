<?php
include('header.php');
$query = "SELECT * FROM vtiger_agent agt INNER JOIN vtiger_crmentity crm on crm.crmid=agt.agentid where crm.deleted=0";

$agent_que = mysql_query($query);
$agent_ops = "";
//echo mysql_num_rows($agent_que);
if (mysql_num_rows($agent_que) > 0) {
    while ($agnt_res = mysql_fetch_array($agent_que)) {
        $an = $agnt_res['agent_first_name'] . ' ' . $agnt_res['agent_last_name'];
        $agent_ops .= "<option value=\"$an\">$an</option>";
    }
}
?>

<section id="contact-page" class="container">

    <!-- FORM 1 [START] -->
    <div class="row-fluid" id="enrlform1" style="display:block">

        <div class="span8 frmenl">
            <h3>Data Entry Submission Form</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div>
                            <label> <b>1. Today's Date</b> <span class="err">*</span></label>
                            <select  name="tdm" id="tdm" onchange="javascript:return tdupdate();">
                                <option value=""> </option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>   
                            <select name="tdt" id="tdt" onchange="javascript:return tdupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select aria-required="true" class="fsField fsRequired" name="tdy" id="tdy" onchange="javascript:return tdupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = (date('Y') - 100); $i <= date('Y') + 5; $i++) {
                                    ?><option value="<?= $i ?>"><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="StartDate" value="<?php echo date("m/d/Y") ?>" >
                        </div>


                        <div>
                            <label> <b>2. Application Entry</b> <span class="err">*</span></label>
                            <span>Who completed the application.</span>
                            <select name="entroller"  class="input-block-level" onchange="ade(this.value);">
                                <option value="">Select</option>
                                <option value="1">Agent</option>
                                <option value="2">Data Entry Operator</option>
                            </select>
                        </div>

                        <div class="desf1" style="display:none">	
                            <div style="display:none;" class="don">
                                <label ><b>2.1 Data Operator's Name</b><span class="err">*</span></label>
                                <input type="text" class="input-block-level" placeholder="First Name" name="dofn">
                                <input type="text" class="input-block-level" placeholder="Last Name" name="doln">
                            </div>

                            <div style="display:none;" class="aob">
                                <label> <b>2.2 Agent</b> <span class="err">*</span></label>
                                <select name="entroller"  class="input-block-level"  onchange="entr(this.value);">							
                                    <option value="">Select</option>
                                    <?php echo $agent_ops; ?>
                                </select>
                            </div>

                            <div style="display:none;" class="ec">
                                <label>  <b>3. Enrollment Center</b> <span class="err">*</span></label>
                                <span>Select the Enrollment Center where you signed up Applicant(s).</span>
                                <div class="checkbox">
                                    <label><input type="checkbox" value="Brea" name="entr_center[]" onchange="javascript:return entrcenterchk()">Brea</label>
                                    <label><input type="checkbox" value="Garden Grove" name="entr_center[]" onchange="javascript:return entrcenterchk()">Garden Grove</label>
                                    <label><input type="checkbox" value="Huntington Beach" name="entr_center[]" onchange="javascript:return entrcenterchk()">Huntington Beach</label>
                                    <label><input type="checkbox" value="Long Beach" name="entr_center[]" onchange="javascript:return entrcenterchk()">Long Beach</label>
                                    <label><input type="checkbox" name="entr_center[]"   onchange="javascript:return entrcenterchk()" value="Westminster">Westminster</label>
                                </div>
                            </div>

                            <div class="desf2" style="display:none">
                                <div style="display:none;" class="afec">	
                                    <label><b>4. How did you hear about us/find us</b> <span class="err">*</span></label>
                                    <select name="app_entr_center" class="input-block-level" onchange="aftec(this.value)" >
                                        <option value="">Select</option>
                                        <option value="Answering Service Call Back">Answering Service Call Back</option>
                                        <option value="Covered California Website">Covered California Website</option>
                                        <option value="Dignity Health">Dignity Health</option>
                                        <option value="Drive By">Drive By</option>
                                        <option value="Email Solicitation">Email Solicitation</option>
                                        <option value="Google">Google</option>
                                        <option value="Inbound 800 Phone Call">Inbound 800 Phone Call</option>
                                        <option value="Online Ads">Online Ads</option>
                                        <option value="Radio Solicitation">Radio Solicitation</option>
                                        <option value="Referral">Referral</option>
                                        <option value="Tax Office">Tax Office</option>
                                        <option value="Unknown">Unknown</option>
                                        <option value="Yelp">Yelp</option>
                                        <option value="Not Listed">Not Listed</option>
                                    </select>
                                </div>

                                <div style="display:none;" class="sohaufu">
                                    <label > <b>4. Source of hear about us/find us</b><span class="err">*</span></label>
                                    <span>If a few words, list the source (ie. TV Ads, Mailers).</span>
                                    <input type="text" class="input-block-level" name="sohaufu" id="sohaufu">
                                </div>

                                <div style="display:none;" class="sor">
                                    <label > <b>4. Source of Referral</b><span class="err">*</span></label>
                                    <span>In a few words, Indicate the Source of the Referral</span>
                                    <input type="text" class="input-block-level" placeholder="Applicant was referred by...." name="sor">

                                </div>

                                <div style="display:none;" class="pdl">      
                                    <label><b>5. Preferred Language </b><span class="err">*</span></label>
                                    <span>Please select the language that you communicated with the client.</span>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="p_l[]" value="English">English</label>
                                        <label><input type="checkbox" name="p_l[]" value="Khmer">Khmer</label>
                                        <label><input type="checkbox" name="p_l[]" value="Korean">Korean</label>
                                        <label><input type="checkbox" name="p_l[]" value="Spanish">Spanish</label>
                                        <label><input type="checkbox" name="p_l[]" value="Thai">Thai</label>
                                        <label><input type="checkbox" name="p_l[]" value="Vietnamese">Vietnamese</label>
                                    </div>	
                                </div>


                                <div class="desf3" style="display:none">	
                                    <div style="display:none;" class="tai">	
                                        <label><b>6. The Applicant is</b> <span class="err">*</span></label>
                                        <span>New Application & New BOR = New Applicant(s) to the Enrollment Center and not in our database. Existing Application = Applicant(s) already a client in our database.</span>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="tai[]" onchange="javascript:return taifn();"value="New Application">New Application</label>
                                            <label><input type="checkbox" name="tai[]" onchange="javascript:return taifn();" value="Broker of Record (BOR)">Broker of Record (BOR)</label>
                                            <label><input type="checkbox" name="tai[]" onchange="javascript:return taifn();" value="Existing Application">Existing Application</label>
                                            <label><input type="checkbox" name="tai[]" onchange="javascript:return taifn();" value="Didn't Buy/Follow Up Needed">Didn't Buy/Follow Up Needed</label>
                                        </div>
                                    </div>

                                    <div style="display:none;" class="eapp">	
                                        <label><b>6.1 Existing Application</b> <span class="err">*</span></label>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="eapp[]" onchange="javascript:return eappfn();"value="Update Covered California application">Update Covered California application</label>
                                            <label><input type="checkbox" name="eapp[]" onchange="javascript:return eappfn();" value="Purchase New Products">Purchase New Products</label>

                                        </div>
                                    </div>

                                    <div class="desf4" style="display:none">

                                        <div style="display:none;" class="ap">	
                                            <label><b>6.1 Products Purchased</b> <span class="err">*</span></label>
                                            <span>Select ALL products that the Applicant(s) signed up for.</span>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="a_p[]" onchange="javascript:return apchk();"value="Individual & Family Health">Individual & Family Health</label>
                                                <label><input type="checkbox" name="a_p[]" onchange="javascript:return apchk();" value="Dental">Dental</label>
                                                <label><input type="checkbox" name="a_p[]" onchange="javascript:return apchk();" value="Vision">Vision</label>
                                                <label><input type="checkbox" name="a_p[]" onchange="javascript:return apchk();" value="Accident">Accident</label>
                                                <label><input type="checkbox" name="a_p[]" onchange="javascript:return apchk();" value="Critical Illness">Critical Illness</label>
                                                <label><input type="checkbox" name="a_p[]" onchange="javascript:return apchk();" value="Group Health">Group Health</label>
                                                <label><input type="checkbox" name="a_p[]" onchange="javascript:return apchk();" value="Life">Life</label>
                                                <label><input type="checkbox" name="a_p[]" onchange="javascript:return apchk();" value="Medicare">Medicare</label>
                                                <label><input type="checkbox" name="a_p[]" onchange="javascript:return apchk();" value="Other">Other</label>
                                            </div>
                                        </div>
                                        <div class="desf6" style="display:none">
                                            <div style="display:none;" class="opp">
                                                <label> <b>6.2 Other</b><span class="err">*</span></label>
                                                <input type="text" class="input-block-level" name="opp" id="opp">
                                            </div>
                                            <div class="desf5" style="display:none">
                                                <div class="tnoaohtf" style="display:none">
                                                    <label ><b>7. How many in your household</b> <span class="err">*</span></label>
                                                    <select class="input-block-level" name="tnoaohtf" id="tnoaohtf" onchange="javascript:return tnoaohtffn(this.value);">
                                                        <option value="">Select</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="Not Listed">Not Listed</option>
                                                    </select>
                                                </div>

                                                <div class="hmiyhnl" style="display:none">
                                                    <label><b>7.1 Household</b> <span class="err">*</span></label>
                                                    <input type="number" class="input-block-level" name="hmiyhnl" id="hmiyhnl">
                                                </div>

                                                <div style="display:none;" class="aac">	        
                                                    <label><b>8. Check Applicant(s) applying for coverage</b> <span class="err">*</span></label>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" name="dpdnt[]" value="Primary">Primary</label>
                                                        <label><input type="checkbox" name="dpdnt[]" value="Spouse">Spouse</label>
                                                        <label><input type="checkbox" name="dpdnt[]" value="Dependent (1)">Dependent (1)</label>
                                                        <label><input type="checkbox" name="dpdnt[]" value="Dependent (2)">Dependent (2)</label>
                                                        <label><input type="checkbox" name="dpdnt[]" value="Dependent (3)">Dependent (3)</label>
                                                        <label><input type="checkbox" name="dpdnt[]" value="Dependent (4)">Dependent (4)</label>
                                                        <label><input type="checkbox" name="dpdnt[]" value="Dependent (5)">Dependent (5)</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>	

                                    </div>	
                                </div>	


                            </div>	

                        </div>
                    </div> 

                </div>
                <div class="clearfix"></div>

                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(1);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>

        </div>
    </div>
    <!-- FORM 1 [END] -->	


    <!-- FORM 2 [START] -->
    <div class="row-fluid" id="enrlform2" style="display:none">

        <div class="span8 frmenl">
            <h3>Primary Applicant's Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">
                        <div>
                            <label ><b>1. Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="pafnm" id="pafnm" onChange="javascript:return anfn();">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="palnm" id="palnm" onChange="javascript:return anfn();">
                        </div>

                        <div>		
                            <label><b>2. Applicant's Middle Name</b> <span class="err">*</span></label>
                            <select name="dtpauamn" class="input-block-level" onchange="javascript:return dtpauamnchk(this.value)">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>  	  
                        </div>

                        <div style="display:none;" class="pamn">
                            <label > <b>2.1 Middle Name or Initial</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="Middle Name or Initial" name="pamn" id="pamn">
                        </div>

                        <div>
                            <label > <b>3. Applicant's Phone</b> <span class="err">*</span></label>
                            <span>If none, use 8001234567.</span>
                            <input type="number" class="input-block-level" name="appn" id="appn">
                        </div>

                        <div>
                            <label > <b>3.1 Applicant's Cell Phone Carrier</b> <span class="err">*</span></label>
                            <span>Info used to update consumer on payments and other important information.</span>									
                            <select  class="input-block-level"  name="pacpc" id="pacpc" onchange="javascript:return pacpcfn(this.value);">
                                <option value="">Select</option>
                                <option value="Alltel">Alltel</option>
                                <option value="AT&amp;T">AT&amp;T</option>
                                <option value="Boost Mobile">Boost Mobile</option>
                                <option value="Cricket">Cricket</option>
                                <option value="Metro PCS">Metro PCS</option>
                                <option value="Pre-Paid">Pre-Paid</option>
                                <option value="Sprint">Sprint</option>
                                <option value="T-Mobile">T-Mobile</option>
                                <option value="US Cellular">US Cellular</option>
                                <option value="Verizon">Verizon</option>
                                <option value="Virgin Mobile">Virgin Mobile</option>
                                <option value="Not Listed">Not Listed</option>
                            </select>
                        </div>

                        <div class="pacpcnl" style="display:none">
                            <label><b>3.1 Cell Phone Carrier</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="pacpcnl" id="pacpcnl">
                        </div>

                        <div class="paeao">
                            <label > <b>4. Applicant's Email</b> <span class="err">*</span></label>
                            <select class="input-block-level" name="paeao" id="paeao" onchange="javascript:return paeaofn(this.value);" >
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>           

                        <div style="display:none;" class="paea">
                            <label > <b>4.1 Email Address</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="paea" id="paea">
                        </div>           

                        <div>
                            <label > <b>5. Applicant's Mailing Address </b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="apma1" id="apma1">
                            <input type="text" class="input-block-level" name="apma2" id="apma2">
                        </div> 

                        <div> 
                            <span class="span4">
                                <label ><span>City</span></label>
                                <input type="text" class="input-block-level" name="apmacity" id="apmacity">
                            </span>
                            <span class="span4">
                                <label ><span>State</span></label>
                                <select class="input-block-level"  name="apmastate" id="apmastate">
                                    <option value="">Select</option>
                                    <option value="Alabama">Alabama</option>
                                    <option value="Alaska">Alaska</option>
                                    <option value="Arizona">Arizona</option>
                                    <option value="Arkansas">Arkansas</option>
                                    <option value="California" selected>California</option>
                                    <option value="Colorado">Colorado</option>
                                    <option value="Connecticut">Connecticut</option>
                                    <option value="Delaware">Delaware</option>
                                    <option value="District of Columbia">District of Columbia</option>
                                    <option value="Florida">Florida</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Hawaii">Hawaii</option>
                                    <option value="Idaho">Idaho</option>
                                    <option value="Illinois">Illinois</option>
                                    <option value="Indiana">Indiana</option>
                                    <option value="Iowa">Iowa</option>
                                    <option value="Kansas">Kansas</option>
                                    <option value="Kentucky">Kentucky</option>
                                    <option value="Louisiana">Louisiana</option>
                                    <option value="Maine">Maine</option>
                                    <option value="Maryland">Maryland</option>
                                    <option value="Massachusetts">Massachusetts</option>
                                    <option value="Michigan">Michigan</option>
                                    <option value="Minnesota">Minnesota</option>
                                    <option value="Mississippi">Mississippi</option>
                                    <option value="Missouri">Missouri</option>
                                    <option value="Montana">Montana</option>
                                    <option value="Nebraska">Nebraska</option>
                                    <option value="Nevada">Nevada</option>
                                    <option value="New Hampshire">New Hampshire</option>
                                    <option value="New Jersey">New Jersey</option>
                                    <option value="New Mexico">New Mexico</option>
                                    <option value="New York">New York</option>
                                    <option value="North Carolina">North Carolina</option>
                                    <option value="North Dakota">North Dakota</option>
                                    <option value="Ohio">Ohio</option>
                                    <option value="Oklahoma">Oklahoma</option>
                                    <option value="Oregon">Oregon</option>
                                    <option value="Pennsylvania">Pennsylvania</option>
                                    <option value="Puerto Rico">Puerto Rico</option>
                                    <option value="Rhode Island">Rhode Island</option>
                                    <option value="South Carolina">South Carolina</option>
                                    <option value="South Dakota">South Dakota</option>
                                    <option value="Tennessee">Tennessee</option>
                                    <option value="Texas">Texas</option>
                                    <option value="Utah">Utah</option>
                                    <option value="Vermont">Vermont</option>
                                    <option value="Virginia">Virginia</option>
                                    <option value="Washington">Washington</option>
                                    <option value="West Virginia">West Virginia</option>
                                    <option value="Wisconsin">Wisconsin</option>
                                    <option value="Wyoming">Wyoming</option>
                                </select>
                            </span>

                            <span class="span4">
                                <label ><span>Zip Code</span></label>
                                <input type="text" class="input-block-level" name="apmazipcode" id="apmazipcode">
                            </span> 
                        </div> 	            

                        <div class="clearfix"></div>

                        <div>	
                            <label><b>6. Applicant's Gender</b> <span class="err">*</span></label>
                            <select name="pag" id="pag" class="input-block-level">
                                <option value="">Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>  	  
                        </div>


                        <div>
                            <label> <b>7. Applicant's Date of Birth</b> <span class="err">*</span></label>
                            <select  name="padobm" id="padobm" onchange="javascript:return padobuppdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="padobd" id="padobd" onchange="javascript:return padobuppdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="padoby" id="padoby" onchange="javascript:return padobuppdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = (date('Y') - 100); $i <= date('Y'); $i++) {
                                    ?><option value="<?= $i ?>"><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="padob" >
                        </div>

                        <div>
                            <label > <b>8. Applicant's Last Four Social Security Number</b> <span class="err">*</span></label>
                            <span>If none, use 0000.</span>
                            <input type="text" class="input-block-level" name="palossn" id="palossn" maxlength="4">
                        </div>

                        <!--
                                                If there's a Spouse that's applying for coverage as well, can we have this (9. Applicant's Household Income) field under the Spouse's information on the Spouse page? If it's only the Primary Applicant applying for coverage then this field can stay here as is. Reason being, the income portion of the application that we do is after all the personal data.
                        -->

                        <div>
                            <label><b>9. Household Income</b> <span class="err">*</span></label>
                            <span>Enter Expected Yearly Household Income total.</span>
                            <input type="number" class="input-block-level" name="aheyi" id="aheyi" placeholder="$">
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>

                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(2);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(2);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>

        </div>
    </div>
    <!-- FORM 2 [END] -->	


    <!-- FORM 3 [START] -->
    <div class="row-fluid" id="enrlform3" style="display:none">

        <div class="span8 frmenl">
            <h3>Spouse Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div>
                            <label ><b>1. Spouse's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="sfn" id="sfn">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="sln" id="sln">
                        </div>
                        <div class="clearfix"></div>

                        <div>		
                            <label><b>2. Spouse's Gender</b> <span class="err">*</span></label>
                            <select name="sg" class="input-block-level">
                                <option value="">Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>  	  
                        </div>
                        <div class="clearfix"></div>

                        <div>
                            <label> <b>3. Spouse's Date of Birth</b> <span class="err">*</span></label>
                            <select  name="sdobm" id="sdobm" onchange="javascript:return sdobupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="sdobd" id="sdobd" onchange="javascript:return sdobupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="sdoby" id="sdoby" onchange="javascript:return sdobupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = (date('Y') - 100); $i <= date('Y'); $i++) {
                                    ?><option value="<?= $i ?>"><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="sdob" >
                        </div>
                        <div class="clearfix"></div>

                        <div>
                            <label > <b>4. Spouse's Phone Number</b> <span class="err">*</span></label> 
                            <span>If none, use 8001234567.</span>                            
                            <input type="text" class="input-block-level" name="spn" id="spn" >
                        </div>

                        <div>
                            <label > <b>5. Spouse's Email</b> <span class="err">*</span></label>  
                            <select class="input-block-level" name="spousesemailo" id="spousesemailo" onchange="javascript:return spousesemailofn(this.value);" >
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>

                        <div class="spousesemail" style="display:none;">
                            <label > <b>5.1 Email Address</b> <span class="err">*</span></label>  
                            <input type="text" class="input-block-level" name="spousesemail" id="spousesemail">
                        </div>


                        <div class="clearfix"></div>

                        <!--
                        If there's a Spouse that's applying for coverage as well, can we have this (9. Applicant's Household Income) field under the Spouse's information on the Spouse page? If it's only the Primary Applicant applying for coverage then this field can stay here as is. Reason being, the income portion of the application that we do is after all the personal data.
                        -->

                        <div>
                            <label><b>6. Household Income</b> <span class="err">*</span></label>
                            <span>Enter Expected Yearly Household Income total.</span>
                            <input type="number" class="input-block-level" name="aheyi" id="aheyi" placeholder="$">
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>

                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(3);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(3);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>

        </div>
    </div>
    <!-- FORM 3 [END] -->	


    <!-- FORM 4 [START] -->
    <div class="row-fluid" id="enrlform4" style="display:none">

        <div class="span8 frmenl">
            <h3>Dependent (1) Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div>
                            <label ><b>1. Dependent's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="dfn_1" id="dfn_1">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="dln_1" id="dln_1">
                        </div>
                        <div class="clearfix"></div>

                        <div>		
                            <label><b>2. Dependent's Gender</b> <span class="err">*</span></label>
                            <select name="dg_1" class="input-block-level">
                                <option value="">Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>  	  
                        </div>
                        <div class="clearfix"></div>

                        <div>
                            <label> <b>3. Dependent's Date of Birth</b> <span class="err">*</span></label>
                            <select  name="ddobm_1" id="ddobm_1" onchange="javascript:return ddobupdate_1();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="ddobd_1" id="ddobd_1" onchange="javascript:return ddobupdate_1();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="ddoby_1" id="ddoby_1" onchange="javascript:return ddobupdate_1();">
                                <option value=""> </option>
                                <?php
                                for ($i = (date('Y') - 100); $i <= date('Y'); $i++) {
                                    ?><option value="<?= $i ?>"><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="ddobdp_1" >
                        </div>
                        <div class="clearfix"></div>

                        <!--
                        And add Household Income here instead of Primary Applicant and Spouse if a Dependent needs coverage too.
                        -->

                        <div>
                            <label><b>4. Household Income</b> <span class="err">*</span></label>
                            <span>Enter Expected Yearly Household Income total.</span>
                            <input type="number" class="input-block-level" name="hid_1" id="hid_1" placeholder="$">
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>

                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(4);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(4);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>

        </div>
    </div>
    <!-- FORM 4 [END] -->	


    <!-- FORM 5 [START] -->
    <div class="row-fluid" id="enrlform5" style="display:none">

        <div class="span8 frmenl">
            <h3>Dependent (2) Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div>
                            <label ><b>1. Dependent's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="dfn_2" id="dfn_2">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="dln_2" id="dln_2">
                        </div>
                        <div class="clearfix"></div>

                        <div>		
                            <label><b>2. Dependent's Gender</b> <span class="err">*</span></label>
                            <select name="dg_2" class="input-block-level">
                                <option value="">Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>  	  
                        </div>
                        <div class="clearfix"></div>

                        <div>
                            <label> <b>3. Dependent's Date of Birth</b> <span class="err">*</span></label>
                            <select  name="ddobm_2" id="ddobm_2" onchange="javascript:return ddobupdate_2();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="ddobd_2" id="ddobd_2" onchange="javascript:return ddobupdate_2();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="ddoby_2" id="ddoby_2" onchange="javascript:return ddobupdate_2();">
                                <option value=""> </option>
                                <?php
                                for ($i = (date('Y') - 100); $i <= date('Y'); $i++) {
                                    ?><option value="<?= $i ?>"><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="ddobdp_2" >
                        </div>
                        <div class="clearfix"></div>

                        <!--
                        And add Household Income here instead of Primary Applicant and Spouse if a Dependent needs coverage too.
                        -->

                        <div>
                            <label><b>4. Household Income</b> <span class="err">*</span></label>
                            <span>Enter Expected Yearly Household Income total.</span>
                            <input type="number" class="input-block-level" name="hid_2" id="hid_2" placeholder="$">
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>

                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(5);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(5);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>

        </div>
    </div>
    <!-- FORM 5 [END] -->	


    <!-- FORM 6 [START] -->
    <div class="row-fluid" id="enrlform6" style="display:none">

        <div class="span8 frmenl">
            <h3>Dependent (3) Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div>
                            <label ><b>1. Dependent's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="dfn_3" id="dfn_3">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="dln_3" id="dln_3">
                        </div>
                        <div class="clearfix"></div>

                        <div>		
                            <label><b>2. Dependent's Gender</b> <span class="err">*</span></label>
                            <select name="dg_3" class="input-block-level">
                                <option value="">Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>  	  
                        </div>
                        <div class="clearfix"></div>

                        <div>
                            <label> <b>3. Dependent's Date of Birth</b> <span class="err">*</span></label>
                            <select  name="ddobm_3" id="ddobm_3" onchange="javascript:return ddobupdate_3();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="ddobd_3" id="ddobd_3" onchange="javascript:return ddobupdate_3();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="ddoby_3" id="ddoby_3" onchange="javascript:return ddobupdate_3();">
                                <option value=""> </option>
                                <?php
                                for ($i = (date('Y') - 100); $i <= date('Y'); $i++) {
                                    ?><option value="<?= $i ?>"><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="ddobdp_3" >
                        </div>
                        <div class="clearfix"></div>

                        <!--
                        And add Household Income here instead of Primary Applicant and Spouse if a Dependent needs coverage too.
                        -->

                        <div>
                            <label><b>4. Household Income</b> <span class="err">*</span></label>
                            <span>Enter Expected Yearly Household Income total.</span>
                            <input type="number" class="input-block-level" name="hid_3" id="hid_3" placeholder="$">
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>

                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(6);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(6);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>

        </div>
    </div>
    <!-- FORM 6 [END] -->	


    <!-- FORM 7 [START] -->
    <div class="row-fluid" id="enrlform7" style="display:none">

        <div class="span8 frmenl">
            <h3>Dependent (4) Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div>
                            <label ><b>1. Dependent's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="dfn_4" id="dfn_4">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="dln_4" id="dln_4">
                        </div>
                        <div class="clearfix"></div>

                        <div>		
                            <label><b>2. Dependent's Gender</b> <span class="err">*</span></label>
                            <select name="dg_4" class="input-block-level">
                                <option value="">Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>  	  
                        </div>
                        <div class="clearfix"></div>

                        <div>
                            <label> <b>3. Dependent's Date of Birth</b> <span class="err">*</span></label>
                            <select  name="ddobm_4" id="ddobm_4" onchange="javascript:return ddobupdate_4();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="ddobd_4" id="ddobd_4" onchange="javascript:return ddobupdate_4();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="ddoby_4" id="ddoby_4" onchange="javascript:return ddobupdate_4();">
                                <option value=""> </option>
                                <?php
                                for ($i = (date('Y') - 100); $i <= date('Y'); $i++) {
                                    ?><option value="<?= $i ?>"><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="ddobdp_4" >
                        </div>
                        <div class="clearfix"></div>

                        <!--
                        And add Household Income here instead of Primary Applicant and Spouse if a Dependent needs coverage too.
                        -->

                        <div>
                            <label><b>4. Household Income</b> <span class="err">*</span></label>
                            <span>Enter Expected Yearly Household Income total.</span>
                            <input type="number" class="input-block-level" name="hid_4" id="hid_4" placeholder="$">
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>

                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(7);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(7);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>

        </div>
    </div>
    <!-- FORM 7 [END] -->	


    <!-- FORM 8 [START] -->
    <div class="row-fluid" id="enrlform8" style="display:none">

        <div class="span8 frmenl">
            <h3>Dependent (5) Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div>
                            <label ><b>1. Dependent's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="dfn_5" id="dfn_5">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="dln_5" id="dln_5">
                        </div>
                        <div class="clearfix"></div>

                        <div>		
                            <label><b>2. Dependent's Gender</b> <span class="err">*</span></label>
                            <select name="dg_5" class="input-block-level">
                                <option value="">Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>  	  
                        </div>
                        <div class="clearfix"></div>

                        <div>
                            <label> <b>3. Dependent's Date of Birth</b> <span class="err">*</span></label>
                            <select  name="ddobm_5" id="ddobm_5" onchange="javascript:return ddobupdate_5();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="ddobd_5" id="ddobd_5" onchange="javascript:return ddobupdate_5();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="ddoby_5" id="ddoby_5" onchange="javascript:return ddobupdate_5();">
                                <option value=""> </option>
                                <?php
                                for ($i = (date('Y') - 100); $i <= date('Y'); $i++) {
                                    ?><option value="<?= $i ?>"><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="ddobdp_5" >
                        </div>
                        <div class="clearfix"></div>

                        <!--
                        And add Household Income here instead of Primary Applicant and Spouse if a Dependent needs coverage too.
                        -->

                        <div>
                            <label><b>4. Household Income</b> <span class="err">*</span></label>
                            <span>Enter Expected Yearly Household Income total.</span>
                            <input type="number" class="input-block-level" name="hid_5" id="hid_5" placeholder="$">
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>

                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(8);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(8);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>

        </div>
    </div>
    <!-- FORM 8 [END] -->	


    <!-- FORM 9 [START] -->
    <div class="row-fluid" id="enrlform9" style="display:none">

        <div class="span8 frmenl">
            <h3>New Applicant's Registration</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">


                        <div>
                            <label ><b class="pann">Primary Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="apfnn" id="apfnn">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="aplnn" id="aplnn">
                        </div>

                        <div class="clearfix"></div>							

                        <div>
                            <label ><b>1. Applicant's Covered California case number</b> <span class="err">*</span></label>
                            <span>10 digit case number on Covered California's application.</span>
                            <input type="number" class="input-block-level" placeholder="Example: 5000314943" name="pacn" id="pacn">
                        </div>

                        <div class="clearfix"></div>

                        <div>
                            <label ><b>2. Applicant's Health Insurance Carrier</b> <span class="err">*</span></label>
                            <select class="input-block-level" name="paic" id="paic" onchange="javascript:return paicfn(this.value);">
                                <option value="">Select</option>
                                <option value="Anthem Blue Cross">Anthem Blue Cross</option>
                                <option value="Blue Shield of California">Blue Shield of California</option>
                                <option value="Chinese Community Health Plan">Chinese Community Health Plan</option>
                                <option value="Cigna">Cigna</option>
                                <option value="Health Net">Health Net</option>
                                <option value="LA Care">LA Care</option>
                                <option value="Kaiser Permanente of California">Kaiser Permanente of California</option>
                                <option value="Medi-Cal">Medi-Cal</option>
                                <option value="Moda Health Plan">Moda Health Plan</option>
                                <option value="Molina Healthcare">Molina Healthcare</option>
                                <option value="Oscar">Oscar</option>
                                <option value="Sharp Health Plan">Sharp Health Plan</option>
                                <option value="United Healthcare">United Healthcare</option>
                                <option value="Western Health Advantage">Western Health Advantage</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>

                        <div class="clearfix"></div>

                        <div class="paicnl" style="display:none">
                            <label><b>2.1 Health Insurance Carrier</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="paicnl" id="paicnl">
                        </div>

                        <div class="patli" style="display:none">
                            <label ><b>3. Applicant's Tier Level</b> <span class="err">*</span></label>
                            <select class="input-block-level" name="patli" id="patli">
                                <option value="">Select</option>
                                <option value="Catastrophic">Catastrophic</option>
                                <option value="Bronze">Bronze</option>
                                <option value="Silver 70">Silver 70</option>
                                <option value="Enhanced Silver 73">Enhanced Silver 73</option>
                                <option value="Enhanced Silver 87">Enhanced Silver 87</option>
                                <option value="Enhanced Silver 94">Enhanced Silver 94</option>
                                <option value="Gold">Gold</option>
                                <option value="Platinum">Platinum</option>
                            </select>
                        </div>

                        <!--
                                                For applications that have Dependent(s) seeking coverage when boxes are checked on the first page, after #3 Applicant's Tier Level, can you add a new field?
                                                4. Dependent(s) on Medi-Cal
                                                                        Drop down menu with Yes or No.
                        -->

                        <div class="domc" style="display:none">
                            <label ><b>4. Dependent(s) on Medi-Cal</b> <span class="err">*</span></label>
                            <select class="input-block-level" name="domc" id="domc">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>

                        <div class="clearfix"></div>

                        <div class="atmpws" style="display:none">
                            <label ><b>5. Applicant's Gross Monthly Premium - Without Subsidies</b> <span class="err">*</span></label>
                            <span>What would the Applicant(s) pay with no subsidies.</span>
                            <input type="number" class="input-block-level" name="atmpws" id="atmpws" placeholder="$">
                        </div>

                        <div class="clearfix"></div>

                        <div class="asmpas" style="display:none">
                            <label ><b>6. Applicant's Net Monthly Premium - After Subsidies</b> <span class="err">*</span></label>
                            <span>What will the Applicant(s) monthly cost be with subsidies.</span>
                            <input type="number" class="input-block-level" name="asmpas" id="asmpas" placeholder="$">
                        </div>

                        <div class="clearfix"></div>

                        <div class="psd" style="display:none">
                            <label> <b>7. Policy Effective Date</b> <span class="err">*</span></label>
                            <select  name="psdm" id="psdm" onchange="javascript:return psdupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="psdd" id="psdd" onchange="javascript:return psdupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="psdy" id="psdy" onchange="javascript:return psdupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y'); $i <= (date('Y') + 2); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="psddp" >
                        </div>

                        <div class="clearfix"></div>

                        <div class="apdd" style="display:none">
                            <label> <b>8. Applicant's 1st Payment Due Date</b> <span class="err">*</span></label>
                            <span style="display: block;">Payment due date listed on Covered California application.</span>
                            <select  name="apddm" id="apddm" onchange="javascript:return apddupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="apddd" id="apddd" onchange="javascript:return apddupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="apddy" id="apddy" onchange="javascript:return apddupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y'); $i <= (date('Y') + 1); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="apdddp" >
                        </div>

                        <div class="clearfix"></div>

                        <div class="dtapftmito" style="display:none">
                            <label ><b>9. Made First Payment in the Office</b> <span class="err">*</span></label>
                            <select class="input-block-level" name="dtapftmito" id="dtapftmito">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>

                        <div class="clearfix"></div>                        

                        <div class="dtasufap" style="display:none">
                            <label ><b>10. Signed Up for Monthly Auto Payment</b> <span class="err">*</span></label>
                            <select class="input-block-level" name="dtasufap" id="dtasufap">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>

                        <div class="clearfix"></div>

                        <div class="nardr">	
                            <label><b>11. Documents Requested</b> <span class="err">*</span></label>
                            <select name="nardr" id="nardr" class="input-block-level" onchange="javascript:return nardrfn(this.value)">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>  	  
                        </div>

                        <div class="nar_documents_requested" style="display:none">

                            <div>
                                <label><b>11. Documents</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="catafrbccfaa[]" onchange="javascript:return catafrbccfaachk();"value="Proof of Citizenship">Proof of Citizenship</label>
                                    <label><input type="checkbox" name="catafrbccfaa[]" onchange="javascript:return catafrbccfaachk();" value="Proof of Income">Proof of Income</label>
                                    <label><input type="checkbox" name="catafrbccfaa[]" onchange="javascript:return catafrbccfaachk();" value="Proof of Min Essential Coverage">Proof of Min Essential Coverage</label>
                                    <label><input type="checkbox" name="catafrbccfaa[]" onchange="javascript:return catafrbccfaachk();" value="Proof of Non-Incarceration">Proof of Non-Incarceration</label>                                
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="ccrpocfwa" style="display:none;">
                                <label><b>11.1 Covered California requested "Proof of Citizenship" from which Applicant(s)</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="ccrpocfwa[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="ccrpocfwa[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="ccrpocfwa[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="ccrpocfwa[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="ccrpocfwa[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="ccrpocfwa[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="ccrpocfwa[]" value="Dependent (5)">Dependent (5)</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="caatcnhnptrpocd" style="display:none;">
                                <label><b>11.2 Check ALL Applicant(s) that DID NOT provide the requested "Proof of Citizenship" documentation.</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="caatcnhnptrpocd[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="caatcnhnptrpocd[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="caatcnhnptrpocd[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpocd[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpocd[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpocd[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpocd[]" value="Dependent (5)">Dependent (5)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpocd[]" value="None - All documents uploaded">None - All documents uploaded</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="ccrpoifwp" style="display:none;">
                                <label><b>11.1 Covered California requested "Proof of Income" from which Applicant(s)</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="ccrpoifwp[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="ccrpoifwp[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="ccrpoifwp[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="ccrpoifwp[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="ccrpoifwp[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="ccrpoifwp[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="ccrpoifwp[]" value="Dependent (5)">Dependent (5)</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="caatcnhnptrpoid" style="display:none;">
                                <label><b>11.2 Check ALL Applicant(s) that DID NOT provide the requested "Proof of Income" documentation.</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="caatcnhnptrpoid[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="caatcnhnptrpoid[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="caatcnhnptrpoid[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpoid[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpoid[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpoid[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpoid[]" value="Dependent (5)">Dependent (5)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpoid[]" value="None - All documents uploaded">None - All documents uploaded</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="ccrpomecfwa" style="display:none;">
                                <label><b>11.1 Covered California requested "Proof of Minimum Essential Coverage" from which Applicant(s)</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="ccrpomecfwa[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="ccrpomecfwa[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="ccrpomecfwa[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="ccrpomecfwa[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="ccrpomecfwa[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="ccrpomecfwa[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="ccrpomecfwa[]" value="Dependent (5)">Dependent (5)</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="caatcnhnptrpomecd" style="display:none;">
                                <label><b>11.2 Check ALL Applicant(s) that DID NOT provide the requested "Proof of Minimum Essential Coverage" documentation.</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="caatcnhnptrpomecd[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="caatcnhnptrpomecd[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="caatcnhnptrpomecd[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpomecd[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpomecd[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpomecd[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpomecd[]" value="Dependent (5)">Dependent (5)</label>
                                    <label><input type="checkbox" name="caatcnhnptrpomecd[]" value="None - All documents uploaded">None - All documents uploaded</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="ccrponifwa" style="display:none;">
                                <label><b>11.1 Covered California requested "Proof of Non-Incarceration" from which Applicant(s)</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="ccrponifwa[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="ccrponifwa[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="ccrponifwa[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="ccrponifwa[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="ccrponifwa[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="ccrponifwa[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="ccrponifwa[]" value="Dependent (5)">Dependent (5)</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="caatcnhnptrponid" style="display:none;">
                                <label><b>11.2 Check ALL Applicant(s) that DID NOT provide the requested "Proof of Non-Incarceration" documentation.</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="caatcnhnptrponid[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="caatcnhnptrponid[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="caatcnhnptrponid[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="caatcnhnptrponid[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="caatcnhnptrponid[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="caatcnhnptrponid[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="caatcnhnptrponid[]" value="Dependent (5)">Dependent (5)</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div>
                            <label><b>12. Notes</b></label>
                            <textarea name="narnote" id="narnote" class="input-block-level" ></textarea>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(9);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(9);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 9 [END] -->	


    <!-- FORM 10 [START] -->
    <div class="row-fluid" id="enrlform10" style="display:none">

        <div class="span8 frmenl">
            <h3>Existing Applicant(s) Registration</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div class="clearfix"></div>
                        <div>
                            <label ><b>Primary Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="apfne" id="apfne">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="aplne" id="aplne">
                        </div>
                        <div class="clearfix"></div>

                        <div  class="witarttec">	        
                            <label><b>1. Why are the Applicant(s) returning to the Enrollment Center</b> <span class="err">*</span></label>
                            <div class="checkbox">
                                <label><input type="checkbox" name="witarttec[]" value="Make changes to Covered California application (ie. Contact Information, Income)" onchange="javascript:return witarttecfn();">Make changes to Covered California application (ie. Contact Information, Income)</label>
                                <label><input type="checkbox" name="witarttec[]" value="Upgrade or downgrade their coverage" onchange="javascript:return witarttecfn();">Upgrade or downgrade their coverage</label>
                                <label><input type="checkbox" name="witarttec[]" value="Change their health insurance carrier" onchange="javascript:return witarttecfn();">Change their health insurance carrier</label>
                            </div>
                        </div>                        

                        <div class="clearfix"></div>

                        <div class="atli" style="display:none">	
                            <label><b>1.1 Applicant's Tier Level</b> <span class="err">*</span></label>
                            <select name="atli" id="atli" class="input-block-level">
                                <option value="">Select</option>
                                <option value="Catastrophic">Catastrophic</option>								
                                <option value="Silver 70">Silver 70</option>
                                <option value="Enhanced Silver 73">Enhanced Silver 73</option>
                                <option value="Enhanced Silver 87">Enhanced Silver 87</option>
                                <option value="Enhanced Silver 94">Enhanced Silver 94</option>
                                <option value="Gold">Gold</option>
                                <option value="Platinum">Platinum</option>
                            </select>  	  
                        </div>

                        <div class="clearfix"></div>                        

                        <div class="anic" style="display:none">	
                            <label><b class="anic_hd">1.2 Applicant's New Insurance Carrier</b> <span class="err">*</span></label>
                            <span>Select Applicant(s) NEW Health Insurance Carrier.</span>
                            <select name="anic" id="anic" class="input-block-level" onchange="javascript:return anicfn(this.value)">
                                <option value="">Select</option>
                                <option value="Anthem Blue Cross">Anthem Blue Cross</option>
                                <option value="Blue Shield of California">Blue Shield of California</option>
                                <option value="Chinese Community Health Plan">Chinese Community Health Plan</option>
                                <option value="Cigna">Cigna</option>
                                <option value="Health Net">Health Net</option>
                                <option value="LA Care">LA Care</option>
                                <option value="Kaiser Permanente of California">Kaiser Permanente of California</option>
                                <option value="Medi-Cal">Medi-Cal</option>
                                <option value="Moda Health Plan">Moda Health Plan</option>
                                <option value="Molina Healthcare">Molina Healthcare</option>
                                <option value="Oscar">Oscar</option>
                                <option value="Sharp Health Plan">Sharp Health Plan</option>
                                <option value="United Healthcare">United Healthcare</option>
                                <option value="Western Health Advantage">Western Health Advantage</option>
                                <option value="Not Listed">Not Listed</option>
                            </select>  	  
                        </div>

                        <div class="clearfix"></div>

                        <div class="epaicnl" style="display:none">
                            <label><b class="epaicnl_hd">1.2 Health Insurance Carrier</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="epaicnl" id="epaicnl">
                        </div>


                        <div class="clearfix"></div>

                        <div class="aprdr">	
                            <label><b>2. Documents Requested</b> <span class="err">*</span></label>
                            <select name="aprdr" id="aprdr" class="input-block-level" onchange="javascript:return aprdrfn(this.value)">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>  	  
                        </div>

                        <div class="apr_documents_requested" style="display:none">
                            <div>
                                <label><b>2. Documents</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="eard[]" onchange="javascript:return eardfn();"value="Proof of Citizenship">Proof of Citizenship</label>
                                    <label><input type="checkbox" name="eard[]" onchange="javascript:return eardfn();" value="Proof of Income">Proof of Income</label>
                                    <label><input type="checkbox" name="eard[]" onchange="javascript:return eardfn();" value="Proof of Min Essential Coverage">Proof of Min Essential Coverage</label>
                                    <label><input type="checkbox" name="eard[]" onchange="javascript:return eardfn();" value="Proof of Non-Incarceration">Proof of Non-Incarceration</label>                                
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="aprccrpocfwa" style="display:none;">
                                <label><b>2.1 Covered California requested "Proof of Citizenship" from which Applicant(s)</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="aprccrpocfwa[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="aprccrpocfwa[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="aprccrpocfwa[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="aprccrpocfwa[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="aprccrpocfwa[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="aprccrpocfwa[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="aprccrpocfwa[]" value="Dependent (5)">Dependent (5)</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="aprcaatcnhnptrpocd" style="display:none;">
                                <label><b>2.2 Check ALL Applicant(s) that DID NOT provide the requested "Proof of Citizenship" documentation.</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="aprcaatcnhnptrpocd[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpocd[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpocd[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpocd[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpocd[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpocd[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpocd[]" value="Dependent (5)">Dependent (5)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpocd[]" value="None - All documents uploaded">None - All documents uploaded</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="aprccrpoifwp" style="display:none;">
                                <label><b>2.1 Covered California requested "Proof of Income" from which Applicant(s)</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="aprccrpoifwp[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="aprccrpoifwp[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="aprccrpoifwp[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="aprccrpoifwp[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="aprccrpoifwp[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="aprccrpoifwp[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="aprccrpoifwp[]" value="Dependent (5)">Dependent (5)</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="aprcaatcnhnptrpoid" style="display:none;">
                                <label><b>2.2 Check ALL Applicant(s) that DID NOT provide the requested "Proof of Income" documentation.</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="aprcaatcnhnptrpoid[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpoid[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpoid[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpoid[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpoid[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpoid[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpoid[]" value="Dependent (5)">Dependent (5)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpoid[]" value="None - All documents uploaded">None - All documents uploaded</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="aprccrpomecfwa" style="display:none;">
                                <label><b>2.1 Covered California requested "Proof of Minimum Essential Coverage" from which Applicant(s)</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="aprccrpomecfwa[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="aprccrpomecfwa[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="aprccrpomecfwa[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="aprccrpomecfwa[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="aprccrpomecfwa[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="aprccrpomecfwa[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="aprccrpomecfwa[]" value="Dependent (5)">Dependent (5)</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="aprcaatcnhnptrpomecd" style="display:none;">
                                <label><b>2.2 Check ALL Applicant(s) that DID NOT provide the requested "Proof of Minimum Essential Coverage" documentation.</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="aprcaatcnhnptrpomecd[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpomecd[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpomecd[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpomecd[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpomecd[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpomecd[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpomecd[]" value="Dependent (5)">Dependent (5)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrpomecd[]" value="None - All documents uploaded">None - All documents uploaded</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="aprccrponifwa" style="display:none;">
                                <label><b>2.1 Covered California requested "Proof of Non-Incarceration" from which Applicant(s)</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="aprccrponifwa[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="aprccrponifwa[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="aprccrponifwa[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="aprccrponifwa[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="aprccrponifwa[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="aprccrponifwa[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="aprccrponifwa[]" value="Dependent (5)">Dependent (5)</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="aprcaatcnhnptrponid" style="display:none;">
                                <label><b>2.2 Check ALL Applicant(s) that DID NOT provide the requested "Proof of Non-Incarceration" documentation.</b> <span class="err">*</span></label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="aprcaatcnhnptrponid[]" value="Primary">Primary</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrponid[]" value="Spouse">Spouse</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrponid[]" value="Dependent (1)">Dependent (1)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrponid[]" value="Dependent (2)">Dependent (2)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrponid[]" value="Dependent (3)">Dependent (3)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrponid[]" value="Dependent (4)">Dependent (4)</label>
                                    <label><input type="checkbox" name="aprcaatcnhnptrponid[]" value="Dependent (5)">Dependent (5)</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                        </div>	
                        <div class="clearfix"></div>   

                        <div>
                            <label><b>3. Notes</b></label>
                            <textarea name="earnote" id="earnote" class="input-block-level" ></textarea>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(10);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(10);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 10 [END] -->	


    <!-- FORM 11 [START] -->
    <div class="row-fluid" id="enrlform11" style="display:none">

        <div class="span8 frmenl">
            <h3>BOR Change Request</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div class="clearfix"></div>
                        <div>
                            <label ><b>Primary Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="apfnbor" id="apfnbor">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="aplnbor" id="aplnbor">
                        </div>
                        <div class="clearfix"></div>

                        <div>	        
                            <label><b>1. Applicant's Health Insurance Carrier</b> <span class="err">*</span></label>
                            <span>Select the insurance carrier that the Applicant currently have.</span>
                            <select name="aicbor" id="aicbor" class="input-block-level" onchange="javascript:return aicborfn(this.value);">
                                <option value="">Select</option>
                                <option value="Anthem Blue Cross">Anthem Blue Cross</option>
                                <option value="Blue Shield of California">Blue Shield of California</option>
                                <option value="Chinese Community Health Plan">Chinese Community Health Plan</option>
                                <option value="Cigna">Cigna</option>
                                <option value="Health Net">Health Net</option>
                                <option value="LA Care">LA Care</option>
                                <option value="Kaiser Permanente of California">Kaiser Permanente of California</option>
                                <option value="Medi-Cal">Medi-Cal</option>
                                <option value="Moda Health Plan">Moda Health Plan</option>
                                <option value="Molina Healthcare">Molina Healthcare</option>
                                <option value="Oscar">Oscar</option>
                                <option value="Sharp Health Plan">Sharp Health Plan</option>
                                <option value="United Healthcare">United Healthcare</option>
                                <option value="Western Health Advantage">Western Health Advantage</option>
                                <option value="Other">Other</option>
                            </select>  	
                        </div>   

                        <div class="clearfix"></div>

                        <div class="hicnl" style="display:none">
                            <label><b>1.1 Health Insurance Carrier</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="hicnl" id="hicnl">
                        </div>                     

                        <div class="clearfix"></div> 

                        <div>	        
                            <label><b>2. Was the Broker of Record (BOR) successfully changed in the Covered California website</b> <span class="err">*</span></label>
                            <span>Is George Balteria successfully added as an agent on the application?</span>
                            <select name="wtborscitccw" id="wtborscitccw" class="input-block-level" onchange="javascript:return wtborscitccwfn(this.value);">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>  	
                        </div>                   

                        <div class="clearfix"></div>   

                        <div class="wwtbnscwcc" style="display:none;">
                            <label><b>2.1 Why was the BOR not successfully changed with Covered CA?</b><span class="err">*</span></label>                            
                            <textarea name="wwtbnscwcc" id="wwtbnscwcc" class="input-block-level" placeholder="Please explain why and what needs to be done:"></textarea>
                        </div>   

                        <div class="clearfix"></div> 

                        <div>	        
                            <label><b>3. Are the required Health Insurance Carrier BOR forms completed</b> <span class="err">*</span></label>
                            <span>Fill out the Applicant(s) correct information and signed before emailing it to Arme.</span>
                            <select name="wthicrbfc" id="wthicrbfc" class="input-block-level" >
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>  	
                        </div>  
                        <div class="clearfix"></div>   

                        <div>
                            <label><b>4. Notes</b></label>  
                            <textarea name="bornotes" id="bornotes" class="input-block-level"></textarea>
                        </div>     

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(11);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(11);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 11 [END] -->


    <!-- FORM 12 [START] -->
    <div class="row-fluid" id="enrlform12" style="display:none">

        <div class="span8 frmenl">
            <h3>Group Health Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div class="clearfix"></div>

                        <div>
                            <label ><b>Primary Point of Contact's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="apocfngh" id="apocfngh">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="apoclngh" id="apoclngh">
                        </div>

                        <div class="clearfix"></div>

                        <div>
                            <label><b>1. Company Name</b> <span class="err">*</span></label>
                            <span>Name of company. If a sole proprietor, list individual's name.</span>
                            <input type="text" class="input-block-level" placeholder="Company Name" name="cngh" id="cngh">                            
                        </div>

                        <div class="clearfix"></div>

                        <div>	        
                            <label><b>2. Group Health Insurance Carrier</b> <span class="err">*</span></label>
                            <select name="ghic" id="ghic" class="input-block-level">
                                <option value="">Select</option>
                                <option value="Aetna">Aetna</option>
                                <option value="Anthem Blue Cross">Anthem Blue Cross</option>
                                <option value="Blue Shield of California">Blue Shield of California</option>
                                <option value="Cal Choice">Cal Choice</option>
                                <option value="Cigna">Cigna</option>
                                <option value="Health Net">Health Net</option>
                                <option value="Kaiser Permanente of California">Kaiser Permanente of California</option>
                                <option value="SHOP">SHOP</option>
                                <option value="United Healthcare">United Healthcare</option>							
                            </select>  	
                        </div>   

                        <div class="clearfix"></div>

                        <div>
                            <label><b>3. Total number of Individuals insured</b> <span class="err">*</span></label>
                            <span>Indicate total number of people on the group plan (including spouse and children).</span>
                            <input type="number" class="input-block-level" name="tnoiigh" id="tnoiigh"  >                            
                        </div>                     

                        <div class="clearfix"></div>

                        <div>
                            <label><b>4. Total Monthly Premium</b> <span class="err">*</span></label>
                            <input type="number" class="input-block-level" name="tmpgh" id="tmpgh" placeholder="$">                            
                        </div>                     

                        <div class="clearfix"></div> 

                        <div>
                            <label><b>5. Start or Renewal Date of Coverage</b> <span class="err">*</span></label>
                            <div class="checkbox">
                                <label><input type="checkbox" name="sdofc[]" value="Start Date of Coverage"  onchange="javascript:return sdofcfn();">Start Date of Coverage</label>
                                <label><input type="checkbox" name="sdofc[]" value="Renewal Date of Coverage"  onchange="javascript:return sdofcfn();">Renewal Date of Coverage</label>                                    
                            </div>
                        </div>


                        <div class="clearfix"></div> 

                        <div class="sdrdocgh"  style="display:none">
                            <label> <b>5.1 Start Date of Coverage</b> <span class="err">*</span></label>
                            <select  name="sdrdocghm" id="sdrdocghm" onchange="javascript:return sdrdocghupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="sdrdocghd" id="sdrdocghd" onchange="javascript:return sdrdocghupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="sdrdocghy" id="sdrdocghy" onchange="javascript:return sdrdocghupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y') - 1; $i <= (date('Y') + 1); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="sdrdocghdp" >
                        </div>

                        <div class="clearfix"></div> 

                        <div class="ghrdoc" style="display:none">
                            <label> <b class="rdoc_hd">5.2 Renewal Date of Coverage</b> <span class="err">*</span></label>
                            <select  name="ghrdocm" id="ghrdocm" onchange="javascript:return ghrdocupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="ghrdocd" id="ghrdocd" onchange="javascript:return ghrdocupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="ghrdocy" id="ghrdocy" onchange="javascript:return ghrdocupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y') - 1; $i <= (date('Y') + 1); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="ghrdocdp" >
                        </div>

                        <div>
                            <label><b>6. Notes</b></label>  
                            <textarea name="grphlthnotes" id="grphlthnotes" class="input-block-level"></textarea>
                        </div>     

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(12);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(12);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 12 [END] -->


    <!-- FORM 13 [START] -->
    <div class="row-fluid" id="enrlform13" style="display:none">

        <div class="span8 frmenl">
            <h3>Dental Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div class="clearfix"></div>

                        <div>
                            <label ><b>Primary Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="pafnd" id="pafnd">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="palnd" id="palnd">
                        </div>

                        <div class="clearfix"></div>

                        <div>	        
                            <label><b>1. Dental Insurance Carrier</b> <span class="err">*</span></label>

                            <select class="input-block-level" name="dicde" id="dicde" onchange="javascript:return dicdefn(this.value);">
                                <option value="">Select</option>
                                <option value="Anthem Blue Cross">Anthem Blue Cross</option>
                                <option value="Cal Dental Plan 595">Cal Dental Plan 595</option>
                                <option value="Delta Dental">Delta Dental</option>
                                <option value="Dental Health Services">Dental Health Services</option>
                                <option value="Other">Other</option>
                            </select>
                        </div> 

                        <div class="clearfix"></div>

                        <div class="dicdeo" style="display:none;">
                            <label><b>1.1 Insurance Carrier</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="dicdeo" id="dicdeo">                            
                        </div>    

                        <div class="clearfix"></div>

                        <div>
                            <label><b>2. Total Monthly Premium</b> <span class="err">*</span></label>
                            <input type="number" class="input-block-level" name="tmpd" id="tmpd" placeholder="$" >                            
                        </div>                     

                        <div class="clearfix"></div>


                        <div>
                            <label> <b>3. Policy Effective Date</b> <span class="err">*</span></label>
                            <div class="clearfix"></div>
                            <select  name="sdocdm" id="sdocdm" onchange="javascript:return sdocdupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="sdocdd" id="sdocdd" onchange="javascript:return sdocdupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="sdocdy" id="sdocdy" onchange="javascript:return sdocdupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y') - 1; $i <= (date('Y') + 1); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="sdocddp" >
                        </div>

                        <div>
                            <label><b>4. Notes</b></label>  
                            <textarea name="denotes" id="denotes" class="input-block-level"></textarea>
                        </div>     

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(13);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(13);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 13 [END] -->


    <!-- FORM 14 [START] -->
    <div class="row-fluid" id="enrlform14" style="display:none">

        <div class="span8 frmenl">
            <h3>Vision Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div class="clearfix"></div>

                        <div>
                            <label ><b>Primary Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="pafnv" id="pafnv">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="palnv" id="palnv">
                        </div>

                        <div class="clearfix"></div>

                        <div>	        
                            <label><b>1. Vision Insurance Carrier</b> <span class="err">*</span></label>

                            <select class="input-block-level" name="vicvn" id="vicvn" onchange="javascript:return vicvnfn(this.value);">
                                <option value="">Select</option>
                                <option value="Anthem Blue Cross">Anthem Blue Cross</option>
                                <option value="Morgan White">Morgan White</option>
                                <option value="VSP">VSP</option>
                                <option value="Other">Other</option>
                            </select>
                        </div> 

                        <div class="clearfix"></div>

                        <div class="vicvno" style="display:none;">
                            <label><b>1.1 Vision Insurance Carrier</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="vicvno" id="vicvno">                            
                        </div>    

                        <div class="clearfix"></div>

                        <div>
                            <label><b>2. Total Monthly Premium</b> <span class="err">*</span></label>
                            <input type="number" class="input-block-level" name="tmpv" id="tmpv" placeholder="$" >                            
                        </div>                     

                        <div class="clearfix"></div>


                        <div>
                            <label> <b>3. Policy Effective Date</b> <span class="err">*</span></label>
                            <div class="clearfix"></div>
                            <select  name="pedvem" id="pedvem" onchange="javascript:return pedveupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="pedved" id="pedved" onchange="javascript:return pedveupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="pedvey" id="pedvey" onchange="javascript:return pedveupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y') - 1; $i <= (date('Y') + 1); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="pedvedp" >
                        </div>

                        <div>
                            <label><b>4. Notes</b></label>  
                            <textarea name="venotes" id="venotes" class="input-block-level"></textarea>
                        </div>     

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(14);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(14);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 14 [END] -->


    <!-- FORM 15 [START] -->
    <div class="row-fluid" id="enrlform15" style="display:none">

        <div class="span8 frmenl">
            <h3>Accident Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div class="clearfix"></div>

                        <div>
                            <label ><b>Primary Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="pafna" id="pafna">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="palna" id="palna">
                        </div>

                        <div class="clearfix"></div>

                        <div>	        
                            <label><b>1. Accident Insurance Carrier</b> <span class="err">*</span></label>

                            <select class="input-block-level" name="aca" id="aca" onchange="javascript:return acafn(this.value);">
                                <option value="">Select</option>
                                <option value="Assurity">Assurity</option>
                                <option value="Colorado Bankers">Colorado Bankers</option>
                                <option value="GTL">GTL</option>
                                <option value="Other">Other</option>
                            </select>
                        </div> 

                        <div class="clearfix"></div>

                        <div class="acao" style="display:none;">
                            <label><b>1.1 Accident Insurance Carrier</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="acao" id="acao">                            
                        </div>    

                        <div class="clearfix"></div>

                        <div>
                            <label><b>2. Total Monthly Premium</b> <span class="err">*</span></label>
                            <input type="number" class="input-block-level" name="tmpa" id="tmpa" placeholder="$" >                            
                        </div>                     

                        <div class="clearfix"></div>


                        <div>
                            <label> <b>3. Policy Effective Date</b> <span class="err">*</span></label>
                            <div class="clearfix"></div>
                            <select  name="pedaroem" id="pedaroem" onchange="javascript:return pedaroeupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="pedaroed" id="pedaroed" onchange="javascript:return pedaroeupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="pedaroey" id="pedaroey" onchange="javascript:return pedaroeupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y') - 1; $i <= (date('Y') + 1); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="pedaroedp" >
                        </div>

                        <div>
                            <label><b>4. Notes</b></label>  
                            <textarea name="aeornotes" id="aeornotes" class="input-block-level"></textarea>
                        </div>     

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(15);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(15);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 15 [END] -->


    <!-- FORM 16 [START] -->
    <div class="row-fluid" id="enrlform16" style="display:none">

        <div class="span8 frmenl">
            <h3>Critical Illness Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div class="clearfix"></div>

                        <div>
                            <label ><b>Primary Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="pafnci" id="pafnci">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="palnci" id="palnci">
                        </div>

                        <div class="clearfix"></div>

                        <div>	        
                            <label><b>1. Critical Illness Insurance Carrier</b> <span class="err">*</span></label>
                            <select class="input-block-level" name="ciic" id="ciic" onchange="javascript:return ciicfn(this.value);">
                                <option value="">Select</option>
                                <option value="Assurity">Assurity</option>
                                <option value="Colorado Bankers">Colorado Bankers</option>
                                <option value="GTL">GTL</option>
                                <option value="Other">Other</option>
                            </select>
                        </div> 

                        <div class="clearfix"></div>

                        <div class="ciico" style="display:none;">
                            <label><b>1.1 Critical Illness Insurance Carrier</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="ciico" id="ciico">                            
                        </div>    

                        <div class="clearfix"></div>

                        <div>
                            <label><b>2. Total Monthly Premium</b> <span class="err">*</span></label>
                            <input type="number" class="input-block-level" name="tmpci" id="tmpci" placeholder="$" >                            
                        </div>                     

                        <div class="clearfix"></div>


                        <div>
                            <label> <b>3. Policy Effective Date</b> <span class="err">*</span></label>
                            <div class="clearfix"></div>
                            <select  name="pedacim" id="pedacim" onchange="javascript:return pedaciupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="pedacid" id="pedacid" onchange="javascript:return pedaciupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="pedaciy" id="pedaciy" onchange="javascript:return pedaciupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y') - 1; $i <= (date('Y') + 1); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="pedacidp" >
                        </div>

                        <div>
                            <label><b>4. Notes</b></label>  
                            <textarea name="cinotes" id="cinotes" class="input-block-level"></textarea>
                        </div>     

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(16);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(16);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 16 [END] -->


    <!-- FORM 17 [START] -->
    <div class="row-fluid" id="enrlform17" style="display:none">

        <div class="span8 frmenl">
            <h3>Life Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div class="clearfix"></div>

                        <div>
                            <label ><b>Primary Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="pafnle" id="pafnle">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="palnle" id="palnle">
                        </div>

                        <div class="clearfix"></div>

                        <div>	        
                            <label><b>1. Life Insurance Carrier</b> <span class="err">*</span></label>
                            <select class="input-block-level" name="licle" id="licle" onchange="javascript:return liclefn(this.value);">
                                <option value="">Select</option>
                                <option value="AIG">AIG</option>
                                <option value="Assurity">Assurity</option>
                                <option value="Colorado Bankers">Colorado Bankers</option>
                                <option value="GTL">GTL</option>
                                <option value="North American">North American</option>
                                <option value="Transamerica">Transamerica</option>
                                <option value="Other">Other</option>                                
                            </select>
                        </div> 

                        <div class="clearfix"></div>

                        <div class="licleo" style="display:none;">
                            <label><b>1.1 Life Insurance Carrier</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="licleo" id="licleo">                            
                        </div>    

                        <div class="clearfix"></div>

                        <div>
                            <label><b>2. Total Monthly Premium</b> <span class="err">*</span></label>
                            <input type="number" class="input-block-level" name="tmple" id="tmple" placeholder="$" >                            
                        </div>                     

                        <div class="clearfix"></div>


                        <div>
                            <label> <b>3. Policy Effective Date</b> <span class="err">*</span></label>
                            <div class="clearfix"></div>
                            <select  name="pedalem" id="pedalem" onchange="javascript:return pedaleupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="pedaled" id="pedaled" onchange="javascript:return pedaleupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="pedaley" id="pedaley" onchange="javascript:return pedaleupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y') - 1; $i <= (date('Y') + 1); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="pedaledp" >
                        </div>

                        <div>
                            <label><b>4. Notes</b></label>  
                            <textarea name="lenotes" id="lenotes" class="input-block-level"></textarea>
                        </div>     

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(17);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(17);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 17 [END] -->


    <!-- FORM 18 [START] -->
    <div class="row-fluid" id="enrlform18" style="display:none">

        <div class="span8 frmenl">
            <h3>Medicare Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div class="clearfix"></div>

                        <div>
                            <label ><b>Primary Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="pafnme" id="pafnme">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="palnme" id="palnme">
                        </div>

                        <div class="clearfix"></div>

                        <div>	        
                            <label><b>1. Medicare Insurance Carrier</b> <span class="err">*</span></label>

                            <select class="input-block-level" name="micme" id="micme" onchange="javascript:return micmefn(this.value);">
                                <option value="">Select</option>
                                <option value="Anthem Blue Cross">Anthem Blue Cross</option>
                                <option value="Humana">Humana</option>
                                <option value="SCAN">SCAN</option>
                                <option value="United Healthcare">United Healthcare</option>
                                <option value="Other">Other</option>
                            </select>
                        </div> 

                        <div class="clearfix"></div>

                        <div class="micmeo" style="display:none;">
                            <label><b>1.1 Medicare Insurance Carrier</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" name="micmeo" id="micmeo">                            
                        </div>    

                        <div class="clearfix"></div>

                        <div>
                            <label><b>2. Total Monthly Premium</b> <span class="err">*</span></label>
                            <input type="number" class="input-block-level" name="tmpme" id="tmpme" placeholder="$" >                            
                        </div>                     

                        <div class="clearfix"></div>


                        <div>
                            <label> <b>3. Policy Effective Date</b> <span class="err">*</span></label>
                            <div class="clearfix"></div>
                            <select  name="pedamem" id="pedamem" onchange="javascript:return pedameupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="pedamed" id="pedamed" onchange="javascript:return pedameupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="pedamey" id="pedamey" onchange="javascript:return pedameupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y') - 1; $i <= (date('Y') + 1); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="pedamedp" >
                        </div>

                        <div>
                            <label><b>4. Notes</b></label>  
                            <textarea name="menotes" id="menotes" class="input-block-level"></textarea>
                        </div>     

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(18);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(18);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 18 [END] -->


    <!-- FORM 19 [START] -->
    <div class="row-fluid" id="enrlform19" style="display:none">

        <div class="span8 frmenl">
            <h3>Other Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>
            <div class="status alert alert-success" style="display: none"></div>

            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">
                <div class="row-fluid">
                    <div class="span12">

                        <div class="clearfix"></div>

                        <div>
                            <label ><b>Primary Applicant's Name</b> <span class="err">*</span></label>
                            <input type="text" class="input-block-level" placeholder="First Name" name="pafnoe" id="pafnoe">
                            <input type="text" class="input-block-level" placeholder="Last Name" name="palnoe" id="palnoe">
                        </div>

                        <div class="clearfix"></div>

                        <div>	        
                            <label><b>1. Type of Insurance</b> <span class="err">*</span></label>                            
                            <input type="text" class="input-block-level" name="toi" id="toi">                            
                        </div>   

                        <div>	        
                            <label><b>2. Other Insurance Carrier</b> <span class="err">*</span></label>                            
                            <input type="text" class="input-block-level" name="oeo" id="oeo">                            
                        </div>    

                        <div class="clearfix"></div>

                        <div>
                            <label><b>3. Total Monthly Premium</b> <span class="err">*</span></label>
                            <input type="number" class="input-block-level" name="tmpoe" id="tmpoe" placeholder="$" >                            
                        </div>                     

                        <div class="clearfix"></div>


                        <div>
                            <label> <b>4. Policy Effective Date</b> <span class="err">*</span></label>
                            <div class="clearfix"></div>
                            <select  name="pedaoem" id="pedaoem" onchange="javascript:return pedaoeupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>   
                            <select name="pedaoed" id="pedaoed" onchange="javascript:return pedaoeupdate();">
                                <option value=""> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select name="pedaoey" id="pedaoey" onchange="javascript:return pedaoeupdate();">
                                <option value=""> </option>
                                <?php
                                for ($i = date('Y') - 1; $i <= (date('Y') + 1); $i++) {
                                    ?><option value="<?= $i ?>" <?php echo ($i == date('Y')) ? 'selected' : ''; ?>><?= $i ?></option><?php
                                }
                                ?>
                            </select>
                            <input type="image" src="./images/cal.png" id="pedaoedp" >
                        </div>

                        <div>
                            <label><b>5. Notes</b></label>  
                            <textarea name="oenotes" id="oenotes" class="input-block-level"></textarea>
                        </div>     

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="clearfix"></div>
                <center class="enrl-resume-block">
                    <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
                </center>
                <div class="clearfix"></div>                
                <div class="enrl-btn-block">				
                    <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(19);"><b>Previous</b></button>
                    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(19);"><b>Next</b></button>    
                </div>
                <div class="clearfix"></div>

            </form>            
        </div>
    </div>
    <!-- FORM 19 [END] -->


    <!-- FORM 20 [START] --> 
    <div class="row-fluid" id="enrlform20" style="display:none">
        <div class="span8 frmenl">
            <h3>Verification of Enrollment</h3>
            <p>Fill in Applicant(s) information as you go through the application to ensure accurate information. Double check spelling and numbers with the Applicant(s) before submission.</p>

            <div class="media box enrl-li">		
                <h4 class="enrl-title">Summary</h4>		
                <ul  class="unstyled">
                    <li><h5>Today's Date</h5>07/11/2016</li>
                    <li><h5>Agent/Data Entry Operator</h5>Test_01_F Test_01_L</li>
                    <li><h5>Enrollment Center</h5>Huntington Beach, Westminster, Brea, Garden Grove, Long Beach</li>
                    <li><h5>Primary Applicant's Name</h5>TESTPAFN TESTPALN</li>
                    <li><h5>Applicant's Covered California case number</h5><span class="label label-info" style="font-size:13px;font-weight:bold;">1234567890</span></li>
                    <li><h5>Applicant's Health Insurance Carrier</h5>Moda Health Plan, Inc Chinese Community Health Plan Chinese Community Health Plan</li>
                    <li><h5>Applicant's Net Monthly Premium - After Subsidies</h5>$5.00</li>
                    <li><h5>Policy Effective Date</h5>07/19/2016</li>

                    <li><h5>Documents Requested</h5></li>
                    <ol>
                        <li>
                            <h5>Applicant's missing <span class="label label-warning enrl-lbl">Proof of Residency</span> documentation:</h5>   
                            <p class="enrl-brn-txt">Primary, Spouse, Dependent (1), Dependent (2), Dependent (3), Dependent (4), Dependent (5) Primary, Spouse, Dependent (1), Dependent (2), Dependent (3), Dependent (4), Dependent (5)</p>
                        </li>

                        <li>
                            <h5>Applicant's missing <span class="label label-warning enrl-lbl">Proof of Income</span> documentation:</h5>   
                            <p class="enrl-brn-txt" >Primary, Spouse, Dependent (1), Dependent (2), Dependent (3), Dependent (4), Dependent (5), None Outstanding Primary, Spouse, Dependent (1), Dependent (2), Dependent (3), Dependent (4), Dependent (5)</p>
                        </li>

                        <li>
                            <h5>Applicant's missing <span class="label label-warning enrl-lbl">Proof of Minimal Essential Coverage</span> documentation:</h5>   
                            <p class="enrl-brn-txt">Primary, Spouse, Dependent (1), Dependent (2), Dependent (3), Dependent (4), Dependent (5), None - All documents uploaded 
                                Primary, Spouse, Dependent (1), Dependent (2), Dependent (3), Dependent (4), Dependent (5) Primary, Spouse, Dependent (1), Dependent (2), Dependent (3), Dependent (4), Dependent (5)</p>
                        </li>

                        <li>
                            <h5>Applicant's missing <span class="label label-warning enrl-lbl">Proof of Non Incarceration</span> documentation:</h5>   
                            <p class="enrl-brn-txt">Primary, Spouse, Dependent (1), Dependent (2), Dependent (3), None - All documents uploaded, Dependent (4) Primary, Spouse, Dependent (1), Dependent (2), Dependent (3), Dependent (4), Dependent (5)</p>
                        </li>
                    </ol>
                </ul>

            </div>    
            <div class="clearfix"></div>
            <div class="media box enrl-li">	
                <h4 class="enrl-title">Group Health Enrollment</h4>	
                <ul class="unstyled">
                    <li><h5>Primary Point of Contact's Name</h5>Test_01_F Test_01_L</li>
                    <li><h5>Company Name</h5>TESTCOMPANY</li>
                    <li><h5>Group Health Insurance Carrier</h5>Anthem Blue Cross</li>
                    <li><h5>Total Monthly Premium</h5>$5.00</li>
                    <li><h5>Start Date of Coverage</h5>07/19/2016</li>                    
                    <li><h5>Renewal Date of Coverage</h5>07/19/2016</li>                    
                </ul>                
            </div>
            <div class="clearfix"></div>
            <div class="media box enrl-li">	
                <h4 class="enrl-title">Dental Enrollment</h4>			
                <ul class="unstyled">
                    <li><h5>Dental Insurance Carrier</h5>Anthem Blue Cross</li>
                    <li><h5>Total Monthly Premium</h5>$5.00</li>
                    <li><h5>Policy Effective Date</h5>07/19/2016</li>                    
                </ul>                
            </div>

            <div class="clearfix"></div>
            <div class="media box enrl-li">	
                <h4 class="enrl-title">Vision Enrollment</h4>			
                <ul class="unstyled">
                    <li><h5>Vision Insurance Carrier</h5>Anthem Blue Cross</li>
                    <li><h5>Total Monthly Premium</h5>$5.00</li>
                    <li><h5>Policy Effective Date</h5>07/19/2016</li>                    
                </ul>                
            </div>

            <div class="clearfix"></div>
            <div class="media box enrl-li">	
                <h4 class="enrl-title">Accident Enrollment</h4>			
                <ul class="unstyled">
                    <li><h5>Accident Insurance Carrier</h5>Assurity</li>
                    <li><h5>Total Monthly Premium</h5>$5.00</li>
                    <li><h5>Policy Effective Date</h5>07/19/2016</li>                    
                </ul>                
            </div>

            <div class="clearfix"></div>
            <div class="media box enrl-li">	
                <h4 class="enrl-title">Critical Illness Enrollment</h4>			
                <ul class="unstyled">
                    <li><h5>Critical Illness Insurance Carrier</h5>Assurity</li>
                    <li><h5>Total Monthly Premium</h5>$5.00</li>
                    <li><h5>Policy Effective Date</h5>07/19/2016</li>                    
                </ul>                
            </div>


            <div class="clearfix"></div>
            <div class="media box enrl-li">	
                <h4 class="enrl-title">Life Enrollment</h4>			
                <ul class="unstyled">
                    <li><h5>Life Insurance Carrier</h5>AIG</li>
                    <li><h5>Total Monthly Premium</h5>$5.00</li>
                    <li><h5>Policy Effective Date</h5>07/19/2016</li>                    
                </ul>                
            </div>


            <div class="clearfix"></div>
            <div class="media box enrl-li">	
                <h4 class="enrl-title">Medicare Enrollment</h4>			
                <ul class="unstyled">
                    <li><h5>Medicare Insurance Carrier</h5>Anthem Blue Cross</li>
                    <li><h5>Total Monthly Premium</h5>$5.00</li>
                    <li><h5>Policy Effective Date</h5>07/19/2016</li>                    
                </ul>                
            </div>


            <div class="clearfix"></div>
            <div class="media box enrl-li">	
                <h4 class="enrl-title">Other Enrollment</h4>			
                <ul class="unstyled">
                    <li><h5>Type of Insurance</h5>TEST</li>
                    <li><h5>Other Insurance Carrier</h5>TEST</li>
                    <li><h5>Total Monthly Premium</h5>$5.00</li>
                    <li><h5>Policy Effective Date</h5>07/19/2016</li>                    
                </ul>                
            </div>

            <div class="clearfix"></div>
            <center class="enrl-resume-block">
                <a href="javascript:void(0)"><strong>Save and Resume Later</strong></a>
            </center>
            <div class="clearfix"></div>                
            <div class="enrl-btn-block">				
                <button type="submit" class="btn btn-primary pull-left" onclick="javascript:return prvsfn(20);"><b>Previous</b></button>
                <button type="submit" class="btn btn-primary pull-right" onclick="javascript:return nextfn(20);"><b>Submit Form</b></button>    
            </div>
            <div class="clearfix"></div>

        </div>
    </div>
    <!-- FORM 20 [END] -->

    <!-- FORM 21 [START] --> 
    <div class="row-fluid" id="enrlform21" style="display:none;min-height:500px;">
        <div class="row-fluid center clearfix" id="pricing-table"> 
            <ul class="plan plan1">
                <li class="plan-name">
                    <h1>Thank You</h1>
                </li>
                <li class="plan-price">
                    <i class="icon-check icon-large" style="font-size: 30px"></i>
                </li>
                <li>
                    <strong style="font-size: 28px;">Form was submitted successfully.</strong>
                </li>
                <li>&nbsp;</li>
            </ul>
        </div>
    </div>
    <!-- FORM 21 [END] -->


</section>

<?php include('footer.php'); ?> 
<script src="./js/enrollment.js"></script>  
<script>
                    $(document).ready(function () {
<?php
if (isset($_GET['frm']) && trim($_GET['frm']) != "") {
    ?>
                            var frm = <?php echo (int) trim($_GET['frm']) ?>;
                            if (frm > 0) {
                                $('[id^="enrlform"]').hide();
                                $('#enrlform' + frm).show();
                            }
                            else {
                                $('[id^="enrlform"]').hide();
                                $('#enrlform1').show();
                            }
    <?php
}
?>
                    });
</script>

</body>
</html>
